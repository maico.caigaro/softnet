
<!DOCTYPE html>
<html>
    <head>
        <html lang="pt-br">
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="icon" href="images/favicon.ico" type="image/ico" />
          <title>SoftNet </title>
          <meta name="viewport" content="width=device-width, initial-scale=1.0">

          <link href="../src/css/bootstrap.css" rel="stylesheet">
          <link rel="stylesheet" href="../src/css/custom.css"/>
          <link rel="stylesheet" href="../src/css/jquery-ui.css" type="text/css" />
    
        <style type="text/css">
            
            body {padding-top: 70px; background-color: #2A3F54; color: #169F85; line-height: 1; padding: 5px;}
            .form-login {max-width: 450px; max-height: 350px; margin-left: auto; margin-right: auto; padding: 10px; border-radius: 10px; margin-top: 10%;}
            .btn {display: inline-block; padding: 6px 12px; border: 2px solid #ddd; color: #ddd; text-decoration: none; font-weight: 700; text-transform: uppercase;
                  margin: 15px 5px 0; transition: all .2s; min-width: 100px; text-align: center; font-size: 14px;}
            .btn.primary {border-color: #006400; color: #006400;}
            #formlogin .mensagemerro{color: red; height: 34px;}
            .error{color: red; font-size: 12px;}
            a:link {text-decoration:none; }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="form-login">
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color:  #169F85;">
                        <div class="panel-title" style="color: white">Login do Sistema</div>
                    </div>
                    <br/>
                    <?php 
                        if (isset($_GET['userinvalid']) == 'true'){?>
                            <div class="alert alert-danger">
                                O usuário ou senha inválidos <br/> 
                                Por favor informe usuário e a senha novamente!
                            </div>
                        <?php }
                    ?>
                    <div style="padding-top:5px" class="panel-body">
                        <div style="display:none" id="result" class="alert alert-danger col-sm-12">
                        </div>
                        <?php $acao = "ope.php"; ?>
                        <form  action="<?php echo $acao; ?>" method="post" id="formlogin" name="formLogin" class="form" role="form">
                            <h2 class="form-signin-heading">Login</h2>
                            <label for="usuario" class="sr-only">Usuário</label>
                                <input type="text" autocomplete="off" id="usuario" name="usuario" id="usuario" class="form-control" placeholder="Informe seu Usuário">
                            <br/>
                            <label for="inputPassword" class="sr-only">Senha</label>
                            <input type="password" id="senha" autocomplete="off" name="senha" class="form-control" placeholder="Informe sua Senha">
                            <div class="checkbox"></div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-12 controls">
                                        <input type="submit" value="Acessar" class="btn btn-success"/>
                                        <input class="btn btn-info" type="reset" value="Limpar" />
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script src="../src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
            <script src="../src/js/jquery.validate.min.js" type="text/javascript"></script>
            
            <script>
                $("#formlogin").validate({
                    rules: {
                        usuario: {
                            required: true,
                            maxlength: 50
                        },
                        senha: {
                            required: true,
                            maxlength: 50
                        }
                    },
                    messages: {
                        usuario: {
                            required: "Por favor, informe o seu usuário",
                            maxlength: "O usuário deve ter no máximo 50 caracteres"
                        },
                        senha: {
                            required: "Por favor, informe a sua Senha",
                            maxlength: "A Senha deve ter no máximo 50 caracteres"
                        }
                    }
                });
            </script>
    </body>
</html>