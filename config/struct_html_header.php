<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="images/favicon.ico" type="image/ico" />
<title>Softnet </title>
<?php
    //Pegar url base
    $url_base= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<link href="<?php echo $url_base?>/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo $url_base?>/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="<?php echo $url_base?>/vendors/nprogress/nprogress.css" rel="stylesheet">
<link href="<?php echo $url_base?>/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
<link href="<?php echo $url_base?>/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
<link href="<?php echo $url_base?>/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
<link href="<?php echo $url_base?>/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="<?php echo $url_base?>/src/css/custom.min.css" rel="stylesheet">

