CREATE TABLE estado
(
  id serial primary key,
  nome character varying(50) NOT NULL,
  uf character varying(2) NOT NULL,
  CONSTRAINT estado_nome_key UNIQUE (nome),
  CONSTRAINT estado_uf_key UNIQUE (uf)
);

CREATE TABLE cidade
(
  id serial primary key,
  nome character varying(80) NOT NULL,
  idestado integer NOT NULL,
  CONSTRAINT cidade_idestado_fkey FOREIGN KEY (idestado)
      REFERENCES public.estado (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE tiposervicos
(
  id serial primary key,
  descricao character varying(100) NOT NULL,
  CONSTRAINT tiposervicos_descricao_key UNIQUE (descricao)
);

create table servico(
id serial primary key,
tipoplano varchar(100) not null,
velocidadeplano varchar(30) not null,
valorplano numeric(16,2) not null);

CREATE TABLE endereco
(
  id serial primary key,
  bairro character varying(100) NOT NULL,
  rua character varying(100) NOT NULL,
  numero integer NOT NULL,
  cep character varying(10) NOT NULL,
  pontoreferencia character varying(100),
  complemento character varying(100),
  ativo character(1) NOT NULL DEFAULT 'S'::bpchar,
  observacao character varying(2000),
  idcidade integer NOT NULL,
  CONSTRAINT endereco_idcidade_fkey FOREIGN KEY (idcidade)
      REFERENCES public.cidade (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE pessoa
(
  id serial primary key,
  nome character varying(80) NOT NULL,
  telefone character varying(20) NOT NULL,
  celular character varying(20),
  observacao character varying(1000),
  email character varying(100) NOT NULL,
  usuario character varying(50) NOT NULL,
  senha character varying(50) NOT NULL,
  idendereco integer NOT NULL,
  CONSTRAINT pessoa_idendereco_fkey FOREIGN KEY (idendereco)
      REFERENCES public.endereco (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT pessoa_usuario_key UNIQUE (usuario)
);

CREATE TABLE pessoafisica
(
  id serial primary key,
  cpf character varying(14) NOT NULL,
  rg character varying(10) NOT NULL,
  idpessoa integer NOT NULL,
  datanascimento date NOT NULL,
  idade integer NOT NULL,
  CONSTRAINT pessoafisica_idpessoa_fkey FOREIGN KEY (idpessoa)
      REFERENCES public.pessoa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT pessoafisica_idpessoa_fkey1 FOREIGN KEY (idpessoa)
      REFERENCES public.pessoa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT pessoafisica_cpf_key UNIQUE (cpf),
  CONSTRAINT pessoafisica_rg_key UNIQUE (rg)
);

CREATE TABLE pessoajuridica
(
  id serial primary key,
  cnpj character varying(18) NOT NULL,
  razaosocial character varying(100) NOT NULL,
  inscricaoestadual character varying(100) NOT NULL,
  idpessoa integer NOT NULL,
  dataabertura date NOT NULL,
  CONSTRAINT pessoajuridica_cnpj_key UNIQUE (cnpj)
);

CREATE TABLE agendamento
(
  id serial primary key,
  descricao character varying,
  dataabertura date NOT NULL,
  turnoinstalado character varying(20),
  status char(1) not null,
  idtiposervico integer NOT NULL,
  idendereco integer NOT NULL,
  idservico integer NOT NULL,
  idpessoa int not null,
  CONSTRAINT agendamento_idendereco_fkey FOREIGN KEY (idendereco)
      REFERENCES public.endereco (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT agendamento_idservico_fkey FOREIGN KEY (idservico)
      REFERENCES public.servico (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT agendamento_idtiposervico_fkey FOREIGN KEY (idtiposervico)
      REFERENCES public.tiposervicos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT agendamento_idpessoa_fkey FOREIGN KEY (idpessoa)
      REFERENCES public.pessoa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE contrato
(
  id serial primary key,
  datacontrato date NOT NULL,
  datacancelamento date,
  motivocancelamento character varying(200) NOT NULL,
  observacao character varying(2000),
  conectadonarede character(1) DEFAULT 'S'::bpchar,
  valormulta numeric(16,2),
  idagendamento integer NOT NULL,
  CONSTRAINT contrato_idagendamento_fkey FOREIGN KEY (idagendamento)
      REFERENCES public.agendamento (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE boleto
(
  id serial primary key,
  valor numeric(16,2) NOT NULL,
  datavencimento date NOT NULL,
  datapagamento date,
  idpessoa integer NOT NULL,
  idtiposervico integer NOT NULL,
  idcontrato integer NOT NULL,
  CONSTRAINT boleto_idcontrato_fkey FOREIGN KEY (idcontrato)
      REFERENCES public.contrato (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT boleto_idpessoa_fkey FOREIGN KEY (idpessoa)
      REFERENCES public.pessoa (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT boleto_idtiposervico_fkey FOREIGN KEY (idtiposervico)
      REFERENCES public.tiposervicos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);