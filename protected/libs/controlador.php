<?php

require_once './protected/conexao.php';
Class Controlador extends Conexao{
    function __construct() {
        parent::__construct();
        
        if(isset($_GET['controle'])){
            $ctrlNome = $_GET['controle'];
            
            $arquivo  = './protected/controller/'.$ctrlNome.'.php';
            if(file_exists($arquivo)){
                
                require $arquivo;
                
                $controle = new $ctrlNome();              
                
                if($_GET['acao']=="novo" || $_GET['acao']=="listar"){
                    $controle->{$_GET['acao']}();
                }else if($_GET['acao']=="inserir" || $_GET['acao']=="atualizar" || $_GET['acao']=="filtrarboleto" 
                        || $_GET['acao']=="finalizaragendamento" || $_GET['acao']=="pagarboleto"){
                    $controle->{$_GET['acao']}($_POST);
                }else if($_GET['acao']=="buscar" || $_GET['acao']=="excluir"){
                    $controle->{$_GET['acao']}($_GET['id']);
                }
            }
        }else{ ?>
            <div id="content">
                <div id="content-header">
                  <div id="breadcrumb"> 
                      <a href="#" class="current"><i class="icon-home"></i>Home</a> </div>
                  <h1>Bem Vindo ao Painel Administrativo</h1>
                </div>
                <div class="container-fluid"><hr>
                  <div class="row-fluid">
                    <div class="span12">
                        <div style="width:50%; margin-top: 4%; margin-left: 20%; margin-right: 20%">
                            
                        </div>
                    </div>
                  </div>
                </div>
              </div>
        <?php }
    }
}
?>