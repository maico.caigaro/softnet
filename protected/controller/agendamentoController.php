<?php

class AgendamentoController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/servicoModel.php';
        require './protected/model/tiposervicoModel.php';
        require './protected/model/agendamentoModel.php';
        $this->servicoModel = new ServicoModel();
        $this->tiposervicoModel = new TiposervicoModel();
        $this->model = new AgendamentoModel();
    }
    
    public function novo() {
        $idenderecopessoabusca = base64_decode($_GET['id']);
        $listaTiposervicos = $this->tiposervicoModel->buscarTodos();
        $listaServicos = $this->servicoModel->buscarTodos();
        $listaDados = $this->model->buscarAgendamentoPessoa($idenderecopessoabusca);
        $acao = 'index.php?controle=agendamentoController&acao=inserir';
        require './protected/view/agendamento/formAgendamento.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "insertsucess";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=agendamentoController&acao=listar&insert=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "inserterror";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=agendamentoController&acao=listar&inserterror=$encriptografa'</script>";
        }
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/agendamento/listAgendamento.php';
    }
    
    public function finalizaragendamento() {
        $id = $_GET['id'];
        $iddecode = base64_decode($id);
        $this->model->finalizaragendamento($iddecode);
    }
}