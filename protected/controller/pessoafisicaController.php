<?php

class PessoafisicaController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/pessoafisicaModel.php';
        require './protected/model/cidadeModel.php';
        $this->cidadeModel = new CidadeModel();
        $this->model = new PessoafisicaModel();
    }
    
    public function novo() {
        $listaCidades = $this->cidadeModel->buscarTodos();
        $acao = 'index.php?controle=pessoafisicaController&acao=inserir';
        require './protected/view/pessoafisica/formPessoafisica.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "insertsucess";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=pessoafisicaController&acao=listar&insert=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "inserterror";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=pessoafisicaController&acao=listar&inserterror=$encriptografa'</script>";
        }
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/pessoafisica/listPessoafisica.php';
    }
    
    public function buscar($id) {
        $iddecode = base64_decode($id);
        $pessoafisica = $this->model->buscar($iddecode);
        
        $listaCidades = $this->cidadeModel->buscarTodos();
        $acao = 'index.php?controle=pessoafisicaController&acao=atualizar';
        require './protected/view/pessoafisica/formPessoafisica.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r == 1){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "updatecerto";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=pessoafisicaController&acao=listar&updt=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "updateerro";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=pessoafisicaController&acao=listar&updterror=$encriptografa'</script>";
        }
    }
    
    public function excluir($id){
        $iddecode = base64_decode($id);
        $r = $this->model->excluir($iddecode);
        
        if($r){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "deletesucess";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=pessoafisicaController&acao=listar&dlt=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "deleteerror";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=pessoafisicaController&acao=listar&dlterror=$encriptografa'</script>";
        }
    }
}