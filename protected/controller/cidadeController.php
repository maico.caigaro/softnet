<?php

class CidadeController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/cidadeModel.php';
        require './protected/model/estadoModel.php';
        $this->estadoModel = new EstadoModel();
        $this->model = new CidadeModel();
    }
    
    public function novo() {
        $listaEstados = $this->estadoModel->buscarTodos();
        $acao = 'index.php?controle=cidadeController&acao=inserir';
        require './protected/view/cidade/formCidade.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "insertsucess";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=cidadeController&acao=listar&insert=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "inserterror";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=cidadeController&acao=listar&inserterror=$encriptografa'</script>";
        }
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/cidade/listCidade.php';
    }
    
    public function buscar($id) {
        $iddecode = base64_decode($id);
        $cidade = $this->model->buscar($iddecode);
        
        $listaEstados = $this->estadoModel->buscarTodos();
        $acao = 'index.php?controle=cidadeController&acao=atualizar';
        require './protected/view/cidade/formCidade.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r == 1){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "updatestatus";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=cidadeController&acao=listar&updt=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "updateerro";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=cidadeController&acao=listar&updterror=$encriptografa'</script>";
        }
        
    }
    
    public function excluir($id){
        $iddecode = base64_decode($id);
        $r = $this->model->excluir($iddecode);
        
        if($r){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "deletesucess";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=cidadeController&acao=listar&dlt=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "deleteerror";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=cidadeController&acao=listar&dlterror=$encriptografa'</script>";
        }
    }
}