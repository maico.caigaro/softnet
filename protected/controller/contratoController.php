<?php

class ContratoController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/contratoModel.php';
        require './protected/model/agendamentoModel.php';
        $this->agendamentoModel = new AgendamentoModel();
        $this->model = new ContratoModel();
    }
    
    public function novo() {
        $listaAgendamentos = $this->agendamentoModel->buscarTodos();
        $acao = 'index.php?controle=contratoController&acao=inserir';
        require './protected/view/contrato/formContrato.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "insertsucess";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=contratoController&acao=listar&insert=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "inserterror";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=contratoController&acao=listar&inserterror=$encriptografa'</script>";
        }
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/contrato/listContrato.php';
    }
    
    public function buscar($id) {
        $iddecode = base64_decode($id);
        $contrato = $this->model->buscar($iddecode);
        
        $listaAgendamentos = $this->agendamentoModel->buscarTodos();
        $acao = 'index.php?controle=contratoController&acao=atualizar';
        require './protected/view/contrato/formContrato.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r == 1){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "updatestatus";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=contratoController&acao=listar&updt=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "updateerro";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=contratoController&acao=listar&updterror=$encriptografa'</script>";
        }
        
    }
}