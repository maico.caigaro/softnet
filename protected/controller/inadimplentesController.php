<?php

class InadimplentesController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/inadimplentesModel.php';
        $this->model = new InadimplentesModel();
    }
    
    public function listar() {
        $listaDados = $this->model->buscarTodos();
        require './protected/view/inadimplentes/listInadimplentes.php';
    }
}