<?php

class BoletoController {
    private $bd, $model;
    
    function __construct() {
        require './protected/model/pessoaModel.php';
        require './protected/model/boletoModel.php';
        require './protected/model/servicoModel.php';
        require './protected/model/contratoModel.php';
        $this->pessoaModel = new PessoaModel();
        $this->servicoModel = new ServicoModel();
        $this->contratoModel = new ContratoModel();
        $this->model = new BoletoModel();
    }
    
    public function novo() {
        $listaPessoas = $this->pessoaModel->buscarTodos();
        $listaPessoasBoleto = $this->pessoaModel->buscarPessoaBoleto();
        $listaServicos = $this->servicoModel->buscarTodos();
        $listaContratos = $this->contratoModel->buscarTodos();
        $acao = 'index.php?controle=boletoController&acao=inserir';
        require './protected/view/boleto/formBoleto.php';
    }
    
    public function inserir(array $dados) {
        $r = $this->model->inserir($dados);
        if($r){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "insertsucess";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=boletoController&acao=listar&insert=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "inserterror";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=boletoController&acao=listar&inserterror=$encriptografa'</script>";
        }
    }
    
    public function listar() {
        $listaPessoas = $this->pessoaModel->buscarTodos();
        $listaPessoasBoleto = $this->pessoaModel->buscarPessoaBoleto();
        $listaDados = $this->model->buscarTodos();
        $acao = 'index.php?controle=boletoController&acao=filtrarboleto';
        require './protected/view/boleto/listBoleto.php';
    }
    
    public function filtrarboleto() {
        $acao = "index.php?controle=boletoController&acao=filtrarboleto";
        //O primeiro if é para realizar o pagamento do boleto
        if(isset($_GET['id']) != '' || isset($_GET['idpessoa']) != ''){
           
           $idboleto = base64_decode($_GET['id']);
           $idpessoa = isset($_GET['idpessoa']);
           //Função para pagar o boleto
           $this->model->pagarboleto($idboleto);
           
           if((isset($_GET['id']) != null) || (isset($_GET['idpessoa']) != null)) {
                $idboleto = base64_decode($_GET['id']);
                
                $listaDados = $this->model->filtrarpagamentoboleto($idboleto, $idpessoa);
                require './protected/view/boleto/listBoleto.php';
            }
        }else{
            if((isset($_POST['idpessoa']) != null)) {
                $idpessoa = $_POST['idpessoa'];
                $listaDados = $this->model->filtrarboleto($idpessoa);
                $listaPessoasBoleto  = $this->pessoaModel->buscarPessoaBoleto();
                require './protected/view/boleto/listBoleto.php';
            }
        }
    }
    
    public function buscar($id) {
        $iddecode = base64_decode($id);
        $boleto = $this->model->buscar($iddecode);
        
        $listaPessoas = $this->pessoaModel->buscarTodos();
        $listaServicos = $this->servicoModel->buscarTodos();
        $listaContratos = $this->contratoModel->buscarTodos();
        $acao = 'index.php?controle=boletoController&acao=atualizar';
        require './protected/view/boleto/formBoleto.php';
    }
    
    public function atualizar(array $dados) {
        $r = $this->model->atualizar($dados);
        if($r == 1){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "updatestatus";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=boletoController&acao=listar&updt=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "updateerro";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=boletoController&acao=listar&updterror=$encriptografa'</script>";
        }
        
    }
    
    public function excluir($id){
        $iddecode = base64_decode($id);
        $r = $this->model->excluir($iddecode);
        
        if($r){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "deletesucess";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=boletoController&acao=listar&dlt=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "deleteerror";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=boletoController&acao=listar&dlterror=$encriptografa'</script>";
        }
    }
}