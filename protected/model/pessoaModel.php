<?php

class PessoaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function buscarTodos() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "select pe.id,
                       pe.nome as nomepessoa
                  from pessoa pe
                  left join pessoafisica pf
                    on pe.id = pf.idpessoa
                  left join pessoajuridica pj
                    on pe.id = pj.idpessoa
                  order by pe.nome asc;";
        
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscarPessoaBoleto() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "select pe.id,
                       pe.nome as nomepessoa
                  from pessoa pe
                  inner join boleto bo
                    on pe.id = bo.idpessoa
                  left join pessoafisica pf
                    on pe.id = pf.idpessoa
                  left join pessoajuridica pj
                    on pe.id = pj.idpessoa
                 group by pe.id, nomepessoa   
                  order by pe.nome asc;";
        
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
}
