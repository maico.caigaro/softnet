<?php

class PessoafisicaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $nome = $_POST['nome'];
        $telefone = $_POST['telefone']; $celular = $_POST['celular']; $email = $_POST['email'];
        $usuario = $_POST['usuario']; $cpf = $_POST['cpf']; $rg  = $_POST['rg']; $senha = $_POST['senha'];
        $bairro = $_POST['bairro']; $rua = $_POST['rua']; $numero = $_POST['numero']; $cep = $_POST['cep'];
        $pontoreferencia = $_POST['pontoreferencia']; $complemento = $_POST['complemento'];
        $ativo = $_POST['ativo']; $observacao = $_POST['observacao']; $idcidade = $_POST['idcidade'];
        $observacaoendereco = $_POST['observacaoendereco']; $datanascimento = $_POST['datanascimento'];
        $idade = $_POST['idadecalculada'];
       
        //Verificar se já possui o cpf informado cadastrado na base de dados
        $consultacpfunico = "select count(*) as quantidadecpf from pessoafisica where cpf = '$cpf'";
        $consultacpfunico = $this->bd->prepare($consultacpfunico);
        $consultacpfunico->execute();
        
        if ($consultacpfunico->rowCount() > 0) {
            foreach ($consultacpfunico as $rs) {
                $quantidadecpf = $rs["quantidadecpf"];
            }
        }
        
        //Verificar se já possui o RG informado cadastrado na base de dados
        $consultargunico = "select count(*) as quantidaderg from pessoafisica where rg = '$rg'";
        $consultargunico = $this->bd->prepare($consultargunico);
        $consultargunico->execute();
        
        if ($consultargunico->rowCount() > 0) {
            foreach ($consultargunico as $rs) {
                $quantidaderg = $rs["quantidaderg"];
            }
        }
        
        //Verificar se já possui o email informado cadastrado na base de dados
        $consultaemailunico = "select count(*) as quantidadeemail from pessoa where email = '$email'";
        $consultaemailunico = $this->bd->prepare($consultaemailunico);
        $consultaemailunico->execute();
        
        if ($consultaemailunico->rowCount() > 0) {
            foreach ($consultaemailunico as $rs) {
                $quantidadeemail = $rs["quantidadeemail"];
            }
        }
        
        //Verificar se já possui o usuario informado cadastrado na base de dados
        $consultausuariounico = "select count(*) as quantidadeusuario from pessoa where usuario = '$usuario'";
        $consultausuariounico = $this->bd->prepare($consultausuariounico);
        $consultausuariounico->execute();
        
        if ($consultausuariounico->rowCount() > 0) {
            foreach ($consultausuariounico as $rs) {
                $quantidadeusuario = $rs["quantidadeusuario"];
            }
        }
        
        if($quantidadecpf >= 1){
            echo "<script>alert('Este CPF já encontra-se cadastrado na base de dados! Favor informe outro CPF');</script>";
        }else if($quantidaderg >= 1){
            echo "<script>alert('Este RG já encontra-se cadastrado na base de dados! Favor informe outro RG');</script>";
        }else if($quantidadeemail >= 1){
            echo "<script>alert('Este E-mail já encontra-se cadastrado na base de dados! Favor informe outro E-mail');</script>";
        }else if($quantidadeusuario >= 1){
            echo "<script>alert('Este Usuário já encontra-se cadastrado na base de dados! Favor informe outro Usuário');</script>";
        }else{
            $sqlendereco = "INSERT INTO endereco(bairro, rua, numero, cep, pontoreferencia, complemento, ativo, observacao, idcidade) 
                VALUES('$bairro', '$rua', $numero, '$cep', '$pontoreferencia', '$complemento', '$ativo', '$observacaoendereco', $idcidade)";
            unset($dados['id']);
            unset($dados['idcidade']); unset($dados['nome']); unset($dados['bairro']);
            unset($dados['email']); unset($dados['telefone']); unset($dados['celular']);
            unset($dados['usuario']); unset($dados['senha']);
            unset($dados['rg']); unset($dados['cpf']); unset($dados['rua']);
            unset($dados['numero']); unset($dados['cep']); unset($dados['ativo']);
            unset($dados['pontoreferencia']); unset($dados['complemento']); unset($dados['observacao']);
            unset($dados['observacaoendereco']); unset($dados['datanascimento']); unset($dados['idadecalculada']);

            $queryendereco = $this->bd->prepare($sqlendereco);
            
            if($queryendereco->execute($dados) == 1){
                //Consultar o último registro inserido
                $sqlmaxendereco   = "SELECT MAX(id) as idendereco FROM endereco;";
                $resmaxendereco   = $this->bd->prepare($sqlmaxendereco);
                $resmaxendereco->execute();
                if ($resmaxendereco->rowCount() > 0) {
                    foreach ($resmaxendereco as $rsmaxendereco) {
                        $idendereco = $rsmaxendereco["idendereco"];
                    }
                }

                $sqlpessoa = "INSERT INTO pessoa(nome, telefone, celular, observacao, email, usuario, senha, idendereco) 
                        VALUES('$nome','$telefone', '$celular', '$observacao', '$email', '$usuario', '$senha', $idendereco)";
                $querypessoa = $this->bd->prepare($sqlpessoa);
                if($querypessoa->execute($dados) == 1){
                    //Consulta ultimo registro pessoa
                    $sqlmaxpessoa   = "SELECT MAX(id) as idpessoa FROM pessoa;";
                    $resmaxpessoa   = $this->bd->prepare($sqlmaxpessoa);
                    $resmaxpessoa->execute();
                    if ($resmaxpessoa->rowCount() > 0) {
                        foreach ($resmaxpessoa as $rsmaxpessoa) {
                            $idpessoa = $rsmaxpessoa["idpessoa"];
                        }
                    }
                    
                    $sqlpessoafisica = "INSERT INTO pessoafisica(idade, cpf, rg, idpessoa, datanascimento) VALUES($idade, '$cpf', '$rg', $idpessoa, '$datanascimento')";
                    $querypessoafisica = $this->bd->prepare($sqlpessoafisica);
                    return $querypessoafisica->execute($dados);
                }
            }
        }
    }

    public function buscarTodos() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "select ende.id,
                       pe.nome as nomepessoa,
                       to_char(pf.datanascimento, 'dd/mm/yyyy') as datanascimento,
                       CASE WHEN pe.celular <> '' THEN 
                              pe.celular
                         WHEN pe.celular = '' THEN 
                              pe.telefone
                         ELSE 
                             'Não possui contato'
                         END as contatopessoafisica,
                       pe.email,
                       pf.cpf, pf.rg,
                       ci.nome as nomecidade,
                       ende.bairro,
                       ende.numero,
                       ende.ativo
                  from pessoa pe
                 inner join pessoafisica pf
                    on pe.id = pf.idpessoa
                 inner join endereco ende
                    on pe.idendereco = ende.id
                 inner join cidade ci
                    on ende.idcidade = ci.id
                 inner join estado est
                    on ci.idestado = est.id 
                 order by pe.nome asc;";
        
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($iddecode) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = "select pe.nome, 
                        pf.idade,
                        to_char(pf.datanascimento, 'yyyy/mm/dd') as datanascimento,
                        pf.idade,
                        pe.telefone,
                        pe.celular,
                        pe.observacao,
                        pe.email,
                        pe.usuario,
                        pe.senha,
                        ende.observacao as observacaoendereco,
                        ende.bairro,
                        ende.rua,
                        ende.id,
                        ende.numero,
                        ende.cep,
                        ende.pontoreferencia,
                        ende.complemento,
                        ende.ativo,
                        ende.idcidade,
                        pf.cpf,
                        pf.rg,
                        ci.idestado
                   from endereco ende
                  inner join pessoa pe
                     on ende.id = pe.idendereco
                  inner join pessoafisica pf
                     on pe.id = pf.idpessoa
                  inner join cidade ci
                     on ende.idcidade = ci.id
                  inner join estado est
                     on ci.idestado = est.id
                  where ende.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $iddecode));
        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $bairro = $_POST['bairro'];
        $rua = $_POST['rua'];
        $numero = $_POST['numero'];
        $cep = $_POST['cep'];
        $pontoreferencia = $_POST['pontoreferencia'];
        $complemento = $_POST['complemento'];
        $ativo = $_POST['ativo'];
        $observacao = $_POST['observacao'];
        $idcidade = $_POST['idcidade'];
        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $telefone = $_POST['telefone'];
        $celular = $_POST['celular'];
        $usuario = $_POST['usuario'];
        $senha = $_POST['senha'];
        $rg = $_POST['rg'];
        $cpf = $_POST['cpf'];
        $observacaoendereco = $_POST['observacaoendereco'];
        $idade = $_POST['idadecalculada'];
        $datanascimento = $_POST['datanascimento'];
        
        //Verificar se já possui o cpf informado cadastrado na base de dados
        $consultacpfunico = "select count(pf.cpf) as quantidadecpf
                               from endereco ende
                              inner join pessoa pe
                                 on ende.id = pe.idendereco
                              inner join pessoafisica pf
                                 on pf.idpessoa = pe.id
                              WHERE pf.cpf = " . "'" . $cpf . "'
                                and ende.id <> $id";
        $consultacpfunico = $this->bd->prepare($consultacpfunico);
        $consultacpfunico->execute();
        
        if ($consultacpfunico->rowCount() > 0) {
            foreach ($consultacpfunico as $rs) {
                $quantidadecpf = $rs["quantidadecpf"];
            }
        }
        
        //Verificar se já possui o RG informado cadastrado na base de dados
        $consultargunico = "select count(pf.rg) as quantidaderg
                               from endereco ende
                              inner join pessoa pe
                                 on ende.id = pe.idendereco
                              inner join pessoafisica pf
                                 on pf.idpessoa = pe.id
                              WHERE pf.rg = " . "'" . $rg . "'
                                and ende.id <> $id";
        $consultargunico = $this->bd->prepare($consultargunico);
        $consultargunico->execute();
        
        if ($consultargunico->rowCount() > 0) {
            foreach ($consultargunico as $rs) {
                $quantidaderg = $rs["quantidaderg"];
            }
        }
        
        //Verificar se já possui o email informado cadastrado na base de dados
        $consultaemailunico = "select count(pe.email) as quantidadeemail
                                 from endereco ende
                                inner join pessoa pe
                                   on ende.id = pe.idendereco
                                WHERE pe.email = " . "'" . $email . "'
                                  and ende.id <> $id";
        $consultaemailunico = $this->bd->prepare($consultaemailunico);
        $consultaemailunico->execute();
        
        if ($consultaemailunico->rowCount() > 0) {
            foreach ($consultaemailunico as $rs) {
                $quantidadeemail = $rs["quantidadeemail"];
            }
        }
        
        //Verificar se já possui o usuario informado cadastrado na base de dados
        $consultausuariounico = "select count(pe.usuario) as quantidadeusuario
                                   from endereco ende
                                  inner join pessoa pe
                                     on ende.id = pe.idendereco
                                  WHERE pe.usuario = " . "'" . $usuario . "'
                                    and ende.id <> $id";
        $consultausuariounico = $this->bd->prepare($consultausuariounico);
        $consultausuariounico->execute();
        
        if ($consultausuariounico->rowCount() > 0) {
            foreach ($consultausuariounico as $rs) {
                $quantidadeusuario = $rs["quantidadeusuario"];
            }
        }
        
        if($quantidadecpf >= 1){
            echo "<script>alert('Este CPF já encontra-se cadastrado na base de dados! Favor informe outro CPF');</script>";
        }else if($quantidaderg >= 1){
            echo "<script>alert('Este RG já encontra-se cadastrado na base de dados! Favor informe outro RG');</script>";
        }else if($quantidadeemail >= 1){
            echo "<script>alert('Este E-mail já encontra-se cadastrado na base de dados! Favor informe outro E-mail');</script>";
        }else if($quantidadeusuario >= 1){
            echo "<script>alert('Este Usuário já encontra-se cadastrado na base de dados! Favor informe outro Usuário');</script>";
        }else{
        
            $sqlendereco = "UPDATE endereco 
                                SET bairro = '$bairro', 
                                    rua = '$rua',
                                    numero = $numero,
                                    cep = '$cep', 
                                    pontoreferencia = '$pontoreferencia',
                                    complemento = '$complemento',
                                    ativo = '$ativo',
                                    observacao = '$observacaoendereco',
                                    idcidade = $idcidade 
                              WHERE id = $id";
            unset($dados['id']);
            unset($dados['bairro']);
            unset($dados['rua']);
            unset($dados['numero']);
            unset($dados['cep']);
            unset($dados['pontoreferencia']);
            unset($dados['complemento']);
            unset($dados['ativo']);
            unset($dados['observacao']);
            unset($dados['idcidade']);
            unset($dados['idadecalculada']);
            unset($dados['nome']);
            unset($dados['email']);
            unset($dados['telefone']);
            unset($dados['celular']);
            unset($dados['usuario']);
            unset($dados['senha']);
            unset($dados['datanascimento']);
            unset($dados['rg']);
            unset($dados['cpf']);
            unset($dados['status']);
            unset($dados['observacaoendereco']);
            $queryendereco = $this->bd->prepare($sqlendereco);
            if($queryendereco->execute($dados) == 1){
                $sqlpessoa = "UPDATE pessoa 
                                SET nome = '$nome',
                                    telefone = '$telefone', 
                                    celular = '$celular',
                                    observacao = '$observacao',
                                    email = '$email',
                                    usuario = '$usuario',
                                    senha = '$senha',
                                    idendereco = $id
                              WHERE idendereco = $id";
                $querypessoa = $this->bd->prepare($sqlpessoa);
                if($querypessoa->execute($dados) == 1){
                    $verificaregistropessoa = "select p.id as idpessoa 
                                               from pessoa p 
                                              inner join pessoafisica pf 
                                                 on p.id = pf.idpessoa 
                                              where p.idendereco = $id";
                    $sqlverificaregistropessoa = $this->bd->prepare($verificaregistropessoa);
                    if($sqlverificaregistropessoa->execute() == 1){
                        if ($sqlverificaregistropessoa->rowCount() > 0) {
                            foreach ($sqlverificaregistropessoa as $rs) {   
                                $verificaidregistropessoa = $rs["idpessoa"];
                            }
                        }

                        $sqlpessoafisica = "UPDATE pessoafisica 
                                               SET rg = '$rg', 
                                                   cpf = '$cpf',
                                                   datanascimento = '$datanascimento',
                                                   idade = $idade
                                             WHERE idpessoa = $verificaidregistropessoa";
                        $querypessoafisica = $this->bd->prepare($sqlpessoafisica);
                        return $querypessoafisica->execute($dados);
                    }
                }
            }
        }
    }

    public function excluir($iddecode) {
        //Consultar id da pessoa fisica
        $sqlconsultapessoafisica = "select pf.id as idpessoafisica
                                      from endereco ende
                                     inner join pessoa pes 
                                        on ende.id = pes.idendereco
                                     inner join pessoafisica pf
                                        on pes.id = pf.idpessoa
                                     where ende.id = $iddecode";
        $sqlverificaidpessoafisica = $this->bd->prepare($sqlconsultapessoafisica);
        $sqlverificaidpessoafisica->execute();
        if ($sqlverificaidpessoafisica->rowCount() > 0) {
            foreach ($sqlverificaidpessoafisica as $rs) {   
                $idpessoafisica = $rs["idpessoafisica"];
            }
        }
        
        $pessoafisica    = "delete from pessoafisica where id = $idpessoafisica";
        $sqlpessoafisica = $this->bd->prepare($pessoafisica);
        $this->bd->prepare($pessoafisica);
        if($sqlpessoafisica->execute() == 1){
            $pessoa    = "delete from pessoa where idendereco = $iddecode";
            $sqlpessoa = $this->bd->prepare($pessoa);
            $this->bd->prepare($pessoa);
            if($sqlpessoa->execute() == 1){
                $endereco    = "delete from endereco where id = $iddecode";
                $sqlendereco = $this->bd->prepare($endereco);
                $this->bd->prepare($endereco);
                return $sqlendereco->execute();
            }
        }
        
    }

}
