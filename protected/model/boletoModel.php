<?php

class BoletoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        if(isset($_POST['valor'])){
            $valormulta =0.00;
        }else{
            //Input Valor
            //Input Valor Multa
            $inputvalor      = str_replace("R$", "", str_replace("", "", $dados['valor']));
            $formatainput = str_replace(".", "", str_replace("", "", $inputvalor));
            $formatavalor = str_replace(",", ".", str_replace("", "", $formatainput));
            $valor   = $formatavalor;
        }
        
        $sql = "INSERT INTO boleto(valor, datavencimento, datapagamento, idpessoa, idtiposervico, idcontrato) "
                . " VALUES($valor, :datavencimento, :datapagamento, :idpessoa, :idtiposervico, :idcontrato)";
        unset($dados['id']);
        unset($dados['valor']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        $mesatual = date('m');
        $anoatual = date('Y');
        
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "select bo.id,
                       'R$ ' || LTRIM(to_char(bo.valor, '9G999G990D99')) as valor,
                       to_char(bo.datavencimento,'dd/MM/yyyy') as datavencimento,
                       to_char(bo.datapagamento, 'dd/MM/yyyy') as datapagamento,
                       tipo.descricao as descricaotiposervico,
                       to_char(con.datacontrato,'dd/MM/yyyy') as datacontrato,
                       pe.nome as nomepessoa,
                       pe.telefone,
                       pe.email,
                       pe.id as idpessoa
                  from boleto bo
                 inner join tiposervicos tipo
                    on bo.idtiposervico = tipo.id
                 inner join contrato con
                    on bo.idcontrato = con.id
                 inner join agendamento agen
                    on agen.id = con.idagendamento
                 inner join endereco ende
                    on agen.idendereco = ende.id
                 inner join pessoa pe
                    on ende.id = pe.idendereco
                  left join pessoafisica pf
                    on pe.id = pf.idpessoa
                  left join pessoajuridica pj
                    on pe.id = pj.idpessoa
                 where (extract(month from bo.datavencimento) <= $mesatual And extract(year from bo.datavencimento) <= $anoatual)";
        
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function pagarboleto($idboleto) {
        $dataatualpagarboleto = date('d/m/Y');
        $pagarboleto    = "update boleto set datapagamento = '$dataatualpagarboleto' where id = $idboleto";
        $query_pagar_boleto = $this->bd->prepare($pagarboleto);
        $query_pagar_boleto->execute();
        if ($query_pagar_boleto->rowCount() > 0) {
          //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
          $string = "insertsucess";
          $encriptografa = base64_encode($string);
          echo "<script>window.location.href='$urlbase/softnet/index.php?controle=boletoController&acao=listar&insertok=$encriptografa'</script>";
        }else{
          return null;
        }
    }
    
    public function buscar($iddecode) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = "SELECT id, 
                       valor, 
                       to_char(datavencimento, 'dd/MM/yyyy') as datavencimento,
                       to_char(datapagamento, 'dd/MM/yyyy') as datapagamento,
                       idpessoa,
                       idtiposervico,
                       idcontrato
                  FROM boleto 
                 WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $iddecode));

        return $query->fetch();
    }
    
    public function atualizar(array $dados) {
        //Valor formatação
        $formatavalor  = str_replace("R$", "", str_replace("", "", $dados['valor']));
        
        //Valor formatação
        $valor = $formatavalor;
        $verificavalor = substr($valor, -3, 1);
        if($verificavalor == ","){
            $inputformata      = str_replace("R$", "", str_replace("", "", $valor));
            $formatavalor = str_replace(".", "", str_replace("", "", $inputformata));
            $valorformatado = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorformatadofinal   = $valorformatado;
        }else{
            $valor = $dados['valor'];
            $formata      = str_replace("R$", "", str_replace("", "", $valor));
            $removevirgula = str_replace(",", "", $formata);
            $valorformatadofinal = $removevirgula;
        }
        
        $sql = "UPDATE boleto 
                   SET valor = $valorformatadofinal, 
                       datavencimento = :datavencimento,
                       datapagamento = :datapagamento,
                       idpessoa = :idpessoa,
                       idtiposervico = :idtiposervico,
                       idcontrato = :idcontrato
                 WHERE id = :id";

        unset($dados['id']);
        unset($dados['valor']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
    
    public function filtrarboleto($idpessoa) {
        $mesatual = date('m');
        $anoatual = date('Y');
        
        if($idpessoa != null){
           $filtropesquisa = 'where pe.id = ' . " $idpessoa " . " order by bo.id asc";
        }else if($idpessoa == ''){
           $filtropesquisa = 'where (extract(month from bo.datavencimento) = ' . $mesatual . ' and extract(year from bo.datavencimento) = ' . $anoatual . ')'; 
        }

        $sql = "select bo.id,
                       'R$ ' || LTRIM(to_char(bo.valor, '9G999G990D99')) as valor,
                       to_char(bo.datavencimento,'dd/MM/yyyy') as datavencimento,
                       to_char(bo.datapagamento, 'dd/MM/yyyy') as datapagamento,
                       tipo.descricao as descricaotiposervico,
                       to_char(con.datacontrato,'dd/MM/yyyy') as datacontrato,
                       pe.nome as nomepessoa,
                       pe.telefone,
                       pe.email,
                       pe.id as idpessoa
                  from boleto bo
                 inner join tiposervicos tipo
                    on bo.idtiposervico = tipo.id
                 inner join contrato con
                    on bo.idcontrato = con.id
                 inner join agendamento agen
                    on agen.id = con.idagendamento
                 inner join endereco ende
                    on agen.idendereco = ende.id
                 inner join pessoa pe
                    on ende.id = pe.idendereco
                 left join pessoafisica pf
                    on pe.id = pf.idpessoa
                  left join pessoajuridica pj
                    on pe.id = pj.idpessoa
                       $filtropesquisa";
        $sqlconsultaboletopessoa = $this->bd->prepare($sql);
        $sqlconsultaboletopessoa->execute();
        if ($sqlconsultaboletopessoa->rowCount() > 0) {
          return $sqlconsultaboletopessoa;
        }else{
          return null;
        }
    }
    
    public function filtrarpagamentoboleto($idboleto, $idpessoa) {
        $mesatual = date('m');
        $anoatual = date('Y');
        
        if($idpessoa != ''){
            $filtrarporid = 'where pe.id = ' . " $idpessoa ";
        }else{
            $filtrarporid = 'where (extract(month from bo.datavencimento) = ' . $mesatual . '
                               and extract(year from bo.datavencimento) = ' . $anoatual . ')';
        }
        
        if($idboleto != null){
           $filtropesquisa = $filtrarporid . " order by bo.id asc";
        }

        $sql = "select bo.id,
                       'R$ ' || LTRIM(to_char(bo.valor, '9G999G990D99')) as valor,
                       to_char(bo.datavencimento,'dd/MM/yyyy') as datavencimento,
                       to_char(bo.datapagamento, 'dd/MM/yyyy') as datapagamento,
                       tipo.descricao as descricaotiposervico,
                       to_char(con.datacontrato,'dd/MM/yyyy') as datacontrato,
                       pe.nome as nomepessoa,
                       pe.telefone,
                       pe.email,
                       pe.id as idpessoa
                  from boleto bo
                 inner join tiposervicos tipo
                    on bo.idtiposervico = tipo.id
                 inner join contrato con
                    on bo.idcontrato = con.id
                 inner join agendamento agen
                    on agen.id = con.idagendamento
                 inner join endereco ende
                    on agen.idendereco = ende.id
                 inner join pessoa pe
                    on ende.id = pe.idendereco
                 left join pessoafisica pf
                    on pe.id = pf.idpessoa
                  left join pessoajuridica pj
                    on pe.id = pj.idpessoa
                       $filtropesquisa";
        $sqlconsultaboletopessoa = $this->bd->prepare($sql);
        $sqlconsultaboletopessoa->execute();
        if ($sqlconsultaboletopessoa->rowCount() > 0) {
          return $sqlconsultaboletopessoa;
        }else{
          return null;
        }
    }
}
