<?php

class ServicoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $inputvalor      = str_replace("R$", "", str_replace("", "", $dados['valorplano']));
        $formatainput = str_replace(".", "", str_replace("", "", $inputvalor));
        $formatavalor = str_replace(",", ".", str_replace("", "", $formatainput));
        $valor   = $formatavalor;
        
        $sql = "INSERT INTO servico(tipoplano, velocidadeplano, valorplano) VALUES(:tipoplano, :velocidadeplano, '$valor')";
        unset($dados['id']);
        unset($dados['valorplano']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "SELECT id, 
                       tipoplano,
                       velocidadeplano,
                       valorplano
                  FROM servico
                 ORDER BY tipoplano asc;";
        
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($iddecode) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = "SELECT id, 
                       tipoplano,
                       velocidadeplano,
                       valorplano
                  FROM servico 
                 WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $iddecode));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        //Valor formatação
        $formata      = str_replace("R$", "", str_replace("", "", $dados['valorplano']));
        //Valor formatação
        $valorplano = $formata;
        $verificavalorplano = substr($valorplano, -3, 1);
        if($verificavalorplano == ","){
            $inputformata      = str_replace("R$", "", str_replace("", "", $valorplano));
            $formatavalor = str_replace(".", "", str_replace("", "", $inputformata));
            $valorformatado = str_replace(",", ".", str_replace("", "", $formatavalor));
            $valorplanoformatado   = $valorformatado;
        }else{
            $valorplano = $dados['valorplano'];
            $formata      = str_replace("R$", "", str_replace("", "", $valorplano));
            $removevirgula = str_replace(",", "", $formata);
            $valorplanoformatado = $removevirgula;
        }
        
        $id = $_POST['id'];
        $sql = "UPDATE servico 
                   SET tipoplano = :tipoplano,
                       velocidadeplano = :velocidadeplano,
                       valorplano = '$valorplanoformatado'
                 WHERE id = $id";

        unset($dados['id']);
        unset($dados['valorplano']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($iddecode) {
        $sql = "DELETE FROM servico WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $iddecode));
    }

}
