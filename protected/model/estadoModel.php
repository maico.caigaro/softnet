<?php

class EstadoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $uf = strtoupper($_POST['uf']);
        
        $sql = "INSERT INTO estado(nome, uf) "
                . " VALUES(:nome, '$uf')";
        unset($dados['id']);
        unset($dados['uf']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "SELECT id, 
                       nome, 
                       uf
                  FROM estado
                 ORDER BY uf asc;";
        
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($iddecode) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = "SELECT id, 
                       nome, 
                       uf
                  FROM estado 
                 WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $iddecode));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $uf = strtoupper($_POST['uf']);
        $sql = "UPDATE estado 
                   SET nome = :nome, uf = '$uf'
                 WHERE id = $id";

        unset($dados['id']);
        unset($dados['uf']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($iddecode) {
        $sql = "DELETE FROM estado WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $iddecode));
    }

}
