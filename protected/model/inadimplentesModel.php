<?php

class InadimplentesModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function buscarTodos() {
        
        //Após 3 dias o cliente entra como cliente inadimplente e aparece na relação de clientes inadimplentes
        $sql = "select CASE WHEN pe.nome <> '' THEN 
                            pe.nome
                       ELSE 
                           pj.razaosocial
                       END as nomepessoa,
                      CASE WHEN pe.telefone <> '' THEN 
                            pe.telefone
                       ELSE 
                           pe.celular
                       END as contatopessoa,
                       CASE WHEN pe.email <> '' THEN 
                           pe.email
                       ELSE 
                          'E-mail não informado'
                       END as emailpessoa,	
                      to_char(bo.datavencimento, 'dd/MM/yyyy') as datavencimento,
                      'R$ ' || LTRIM(to_char(bo.valor, '9G999G990D99')) as valorboleto,
                      tipo.descricao as descricaotiposervico,
                      (ser.tipoplano || ' - ' || ser.velocidadeplano) as planocontratado
                 from boleto bo
                inner join tiposervicos tipo
                   on bo.idtiposervico = tipo.id
                inner join pessoa pe
                   on bo.idpessoa = pe.id
                 left join pessoafisica pf
                   on pe.id = pf.idpessoa
                 left join pessoajuridica pj
                   on pe.id = pj.idpessoa
                inner join endereco ende
                   on ende.id = pe.idendereco
                inner join agendamento agen
                   on agen.idendereco = ende.id
                inner join servico ser
                   on agen.idservico = ser.id
                WHERE ((CURRENT_DATE - bo.datavencimento) >= 3)
                  and bo.datapagamento is null;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
}
