<?php

class TiposervicoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $sql = "INSERT INTO tiposervicos(descricao) VALUES(:descricao)";
        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "SELECT id, descricao
                  FROM tiposervicos
                 ORDER BY descricao asc;";
        
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($iddecode) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = "SELECT id, 
                       descricao
                  FROM tiposervicos 
                 WHERE id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $iddecode));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $sql = "UPDATE tiposervicos 
                   SET descricao = :descricao
                 WHERE id = $id";

        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($iddecode) {
        $sql = "DELETE FROM tiposervicos WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $iddecode));
    }

}
