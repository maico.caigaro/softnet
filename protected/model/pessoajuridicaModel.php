<?php

class PessoafisicaModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $dataabertura = $_POST['dataabertura'];
        $razaosocial = $_POST['razaosocial']; $email = $_POST['email']; $cnpj = $_POST['cnpj']; 
        $inscricaoestadual = $_POST['inscricaoestadual'];
        $telefone = $_POST['telefone']; $celular = $_POST['celular']; 
        $usuario = $_POST['usuario']; $senha = $_POST['senha']; $ativo = $_POST['ativo'];
        $observacao = $_POST['observacao']; $idcidade = $_POST['idcidade']; $bairro = $_POST['bairro'];
        $rua = $_POST['rua']; $numero = $_POST['numero']; $cep = $_POST['cep'];
        $pontoreferencia = $_POST['pontoreferencia']; $complemento = $_POST['complemento'];
        $observacaoendereco = $_POST['observacaoendereco']; $nome = $_POST['nomefantasia'];
        
        //Verificar se já possui o cnpj informado cadastrado na base de dados
        $consultacnpjunico = "select count(*) as quantidadecnpj from pessoajuridica where cnpj = '$cnpj'";
        $consultacnpjunico = $this->bd->prepare($consultacnpjunico);
        $consultacnpjunico->execute();
        
        if ($consultacnpjunico->rowCount() > 0) {
            foreach ($consultacnpjunico as $rs) {
                $quantidadecnpj = $rs["quantidadecnpj"];
            }
        }
        
        //Verificar se já possui a inscricaoestadual informada cadastrado na base de dados
        $consultainscricaoestadualunico = "select count(*) as quantidadeinscricaoestadual from pessoajuridica where inscricaoestadual = '$inscricaoestadual'";
        $consultainscricaoestadualunico = $this->bd->prepare($consultainscricaoestadualunico);
        $consultainscricaoestadualunico->execute();
        
        if ($consultainscricaoestadualunico->rowCount() > 0) {
            foreach ($consultainscricaoestadualunico as $rs) {
                $quantidadeinscricaoestadual = $rs["quantidadeinscricaoestadual"];
            }
        }
        
        //Verificar se já possui a razaosocial informada cadastrado na base de dados
        $consultarazaosocialunico = "select count(*) as quantidaderazaosocial from pessoajuridica where razaosocial = '$razaosocial'";
        $consultarazaosocialunico = $this->bd->prepare($consultarazaosocialunico);
        $consultarazaosocialunico->execute();
        
        if ($consultarazaosocialunico->rowCount() > 0) {
            foreach ($consultarazaosocialunico as $rs) {
                $quantidaderazaosocial = $rs["quantidaderazaosocial"];
            }
        }

        //Verificar se já possui o email informado cadastrado na base de dados
        $consultaemailunico = "select count(*) as quantidadeemail from pessoa where email = '$email'";
        $consultaemailunico = $this->bd->prepare($consultaemailunico);
        $consultaemailunico->execute();
        
        if ($consultaemailunico->rowCount() > 0) {
            foreach ($consultaemailunico as $rs) {
                $quantidadeemail = $rs["quantidadeemail"];
            }
        }
     
        //Verificar se já possui o usuario informado cadastrado na base de dados
        $consultausuariounico = "select count(*) as quantidadeusuario from pessoa where usuario = '$usuario'";
        $consultausuariounico = $this->bd->prepare($consultausuariounico);
        $consultausuariounico->execute();
        
        if ($consultausuariounico->rowCount() > 0) {
            foreach ($consultausuariounico as $rs) {
                $quantidadeusuario = $rs["quantidadeusuario"];
            }
        }
        if($quantidadecnpj >= 1){
            echo "<script>alert('Este CNPJ já encontra-se cadastrado na base de dados! Favor informe outro CNPJ');</script>";
        }else if($quantidaderazaosocial >= 1){
            echo "<script>alert('Esta Razão Social já encontra-se cadastrada na base de dados!');</script>";
        }else if($quantidadeinscricaoestadual >= 1){
            echo "<script>alert('Esta Inscrição Estadual já encontra-se cadastrada na base de dados!');</script>";
        }else if($quantidadeemail >= 1){
            echo "<script>alert('Este E-mail já encontra-se cadastrado na base de dados! Favor informe outro E-mail');</script>";
        }else if($quantidadeusuario >= 1){
            echo "<script>alert('Este Usuário já encontra-se cadastrado na base de dados! Favor informe outro Usuário');</script>";
        }else{
            $sqlendereco = "INSERT INTO endereco(bairro, rua, numero, cep, pontoreferencia, complemento, ativo, observacao, idcidade) 
                VALUES('$bairro', '$rua', $numero, '$cep', '$pontoreferencia', '$complemento', '$ativo', '$observacao', $idcidade)";
            unset($dados['id']);
            unset($dados['idcidade']); unset($dados['razaosocial']); unset($dados['bairro']);
            unset($dados['email']); unset($dados['telefone']); unset($dados['celular']);
            unset($dados['usuario']); unset($dados['senha']); unset($dados['dataabertura']);
            unset($dados['idade']); unset($dados['cnpj']); unset($dados['rua']);
            unset($dados['numero']); unset($dados['cep']); unset($dados['ativo']);
            unset($dados['pontoreferencia']); unset($dados['complemento']); unset($dados['observacao']);
            unset($dados['observacaoendereco']); unset($dados['inscricaoestadual']); unset($dados['nomefantasia']);

            $queryendereco = $this->bd->prepare($sqlendereco);
            if($queryendereco->execute($dados) == 1){
                //Consultar o último registro inserido
                $sqlmaxendereco   = "SELECT MAX(id) as idendereco FROM endereco;";
                $resmaxendereco   = $this->bd->prepare($sqlmaxendereco);
                $resmaxendereco->execute();
                if ($resmaxendereco->rowCount() > 0) {
                    foreach ($resmaxendereco as $rsmaxendereco) {
                        $idendereco = $rsmaxendereco["idendereco"];
                    }
                }

                $sqlpessoa = "INSERT INTO pessoa(nome, telefone, celular, observacao, email, usuario, senha, idendereco) 
                        VALUES('$nome', '$telefone', '$celular', '$observacaoendereco', '$email', '$usuario', '$senha', $idendereco)";
                $querypessoa = $this->bd->prepare($sqlpessoa);
                if($querypessoa->execute($dados) == 1){
                    //Consulta ultimo registro pessoa
                    $sqlmaxpessoa   = "SELECT MAX(id) as idpessoa FROM pessoa;";
                    $resmaxpessoa   = $this->bd->prepare($sqlmaxpessoa);
                    $resmaxpessoa->execute();
                    if ($resmaxpessoa->rowCount() > 0) {
                        foreach ($resmaxpessoa as $rsmaxpessoa) {
                            $idpessoa = $rsmaxpessoa["idpessoa"];
                        }
                    }

                    $sqlpessoajuridica = "INSERT INTO pessoajuridica(razaosocial, cnpj, inscricaoestadual, idpessoa, dataabertura) VALUES('$razaosocial', '$cnpj', '$inscricaoestadual', $idpessoa, '$dataabertura')";
                    $querypessoajuridica = $this->bd->prepare($sqlpessoajuridica);
                    return $querypessoajuridica->execute($dados);
                }
            }
        }
    }

    public function buscarTodos() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "select ende.id,
                        pj.razaosocial as razaosocial,
                        to_char(pj.dataabertura, 'dd/mm/yyyy') as dataabertura,
                        CASE WHEN pe.celular <> '' THEN 
                               pe.celular
                          WHEN pe.celular = '' THEN 
                               pe.telefone
                          ELSE 
                              'Não possui contato'
                          END as contatopessoajuridica,
                        pe.email,
                        pj.cnpj, 
                        pj.razaosocial,
                        pj.inscricaoestadual,
                        ci.nome as nomecidade,
                        ende.bairro,
                        ende.numero,
                        ende.ativo
                   from pessoa pe
                  inner join pessoajuridica pj
                     on pe.id = pj.idpessoa
                  inner join endereco ende
                     on pe.idendereco = ende.id
                  inner join cidade ci
                     on ende.idcidade = ci.id
                  inner join estado est
                     on ci.idestado = est.id 
                  order by pj.razaosocial asc;";
        
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($iddecode) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = "select ende.id,
                        pj.razaosocial as razaosocial,
                        pe.nome as nomefantasia,
                        to_char(pj.dataabertura, 'dd/mm/yyyy') as dataabertura,
                        CASE WHEN pe.celular <> '' THEN 
                               pe.celular
                          WHEN pe.celular = '' THEN 
                               pe.telefone
                          ELSE 
                              'Não possui contato'
                          END as contatopessoajuridica,
                        pe.celular,
                        pe.telefone,
                        pe.usuario,
                        pe.senha,
                        pe.observacao,
                        pe.email,
                        pj.cnpj, 
                        pj.razaosocial,
                        pj.inscricaoestadual,
                        ci.nome as nomecidade,
                        ende.bairro,
                        ende.numero,
                        ende.ativo,
                        ende.idcidade,
                        ende.rua,
                        ende.cep,
                        ende.pontoreferencia,
                        ende.complemento,
                        ende.observacao as observacaoendereco
                   from pessoa pe
                  inner join pessoajuridica pj
                     on pe.id = pj.idpessoa
                  inner join endereco ende
                     on pe.idendereco = ende.id
                  inner join cidade ci
                     on ende.idcidade = ci.id
                  inner join estado est
                     on ci.idestado = est.id 
                  where ende.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $iddecode));
        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        $razaosocial = $_POST['razaosocial'];
        $nome = $_POST['nomefantasia'];
        $email = $_POST['email'];
        $cnpj = $_POST['cnpj'];
        $inscricaoestadual = $_POST['inscricaoestadual'];
        $dataabertura = $_POST['dataabertura'];
        $telefone = $_POST['telefone'];
        $celular = $_POST['celular'];
        $usuario = $_POST['usuario'];
        $senha = $_POST['senha'];
        $ativo = $_POST['ativo'];
        $observacao = $_POST['observacao'];
        $idcidade = $_POST['idcidade'];
        $bairro = $_POST['bairro'];
        $rua = $_POST['rua'];
        $numero = $_POST['numero'];
        $cep = $_POST['cep'];
        $pontoreferencia = $_POST['pontoreferencia'];
        $complemento = $_POST['complemento'];
        $observacaoendereco = $_POST['observacaoendereco'];
        
        //Verificar se já possui o cnpj informado cadastrado na base de dados
        $consultacnpjunico = "select count(pj.cnpj) as quantidadecnpj
                               from endereco ende
                              inner join pessoa pe
                                 on ende.id = pe.idendereco
                              inner join pessoajuridica pj
                                 on pj.idpessoa = pe.id
                              WHERE pj.cnpj = " . "'" . $cnpj . "'
                                and ende.id <> $id";
        $consultacnpjunico = $this->bd->prepare($consultacnpjunico);
        $consultacnpjunico->execute();
        
        if ($consultacnpjunico->rowCount() > 0) {
            foreach ($consultacnpjunico as $rs) {
                $quantidadecnpj = $rs["quantidadecnpj"];
            }
        }
        
        //Verificar se já possui o nomefantasia informado cadastrado na base de dados
        $consultanomefantasiajunico = "select count(pe.nome) as quantidadenomefantasia
                               from endereco ende
                              inner join pessoa pe
                                 on ende.id = pe.idendereco
                              inner join pessoajuridica pj
                                 on pj.idpessoa = pe.id
                              WHERE pe.nome = " . "'" . $nome . "'
                                and ende.id <> $id";
        $consultanomefantasiajunicounico = $this->bd->prepare($consultanomefantasiajunico);
        $consultanomefantasiajunicounico->execute();
        
        if ($consultanomefantasiajunicounico->rowCount() > 0) {
            foreach ($consultanomefantasiajunicounico as $rs) {
                $quantidadefantasia = $rs["quantidadenomefantasia"];
            }
        }
        
        
        //Verificar se já possui a inscricaoestadual informado cadastrado na base de dados
        $consultainscricaoestadualunico = "select count(pj.inscricaoestadual) as quantidadeinscricaoestadual
                                            from endereco ende
                                           inner join pessoa pe
                                              on ende.id = pe.idendereco
                                           inner join pessoajuridica pj
                                              on pj.idpessoa = pe.id
                                           WHERE pj.inscricaoestadual = " . "'" . $inscricaoestadual . "'
                                             and ende.id <> $id";
        $consultainscricaoestadualunico = $this->bd->prepare($consultainscricaoestadualunico);
        $consultainscricaoestadualunico->execute();
        
        if ($consultainscricaoestadualunico->rowCount() > 0) {
            foreach ($consultainscricaoestadualunico as $rs) {
                $quantidadeinscricaoestadual = $rs["quantidadeinscricaoestadual"];
            }
        }
        
        //Verificar se já possui a razaosocial informado cadastrado na base de dados
        $consultarazaosocialunico = "select count(pj.razaosocial) as quantidaderazaosocial
                                        from endereco ende
                                       inner join pessoa pe
                                          on ende.id = pe.idendereco
                                       inner join pessoajuridica pj
                                          on pj.idpessoa = pe.id
                                       WHERE pj.razaosocial = " . "'" . $razaosocial . "'
                                         and ende.id <> $id";
        $consultarazaosocialunico = $this->bd->prepare($consultarazaosocialunico);
        $consultarazaosocialunico->execute();
        
        if ($consultarazaosocialunico->rowCount() > 0) {
            foreach ($consultarazaosocialunico as $rs) {
                $quantidaderazaosocial = $rs["quantidaderazaosocial"];
            }
        }
        
        //Verificar se já possui o email informado cadastrado na base de dados
        $consultaemailunico = "select count(pe.email) as quantidadeemail
                                 from endereco ende
                                inner join pessoa pe
                                   on ende.id = pe.idendereco
                                WHERE pe.email = " . "'" . $email . "'
                                  and ende.id <> $id";
        $consultaemailunico = $this->bd->prepare($consultaemailunico);
        $consultaemailunico->execute();
        
        if ($consultaemailunico->rowCount() > 0) {
            foreach ($consultaemailunico as $rs) {
                $quantidadeemail = $rs["quantidadeemail"];
            }
        }
        
        //Verificar se já possui o usuario informado cadastrado na base de dados
        $consultausuariounico = "select count(pe.usuario) as quantidadeusuario
                                   from endereco ende
                                  inner join pessoa pe
                                     on ende.id = pe.idendereco
                                  WHERE pe.usuario = " . "'" . $usuario . "'
                                    and ende.id <> $id";
        $consultausuariounico = $this->bd->prepare($consultausuariounico);
        $consultausuariounico->execute();
        
        if ($consultausuariounico->rowCount() > 0) {
            foreach ($consultausuariounico as $rs) {
                $quantidadeusuario = $rs["quantidadeusuario"];
            }
        }

        if($quantidadecnpj >= 1){
            echo "<script>alert('Este CNPJ já encontra-se cadastrado na base de dados! Favor informe outro CNPJ');</script>";
        }else if($quantidaderazaosocial >= 1){
            echo "<script>alert('Esta Razão Social já encontra-se cadastrado na base de dados! Favor informe outro Razão Social');</script>";
        }else if($quantidadefantasia >= 1){
            echo "<script>alert('Este Nome Fantasia já encontra-se cadastrado na base de dados!');</script>";
        }else if($quantidadeinscricaoestadual >= 1){
            echo "<script>alert('Esta Inscrição Estadual já encontra-se cadastrado na base de dados!');</script>";
        }else if($quantidadeemail >= 1){
            echo "<script>alert('Este E-mail já encontra-se cadastrado na base de dados! Favor informe outro E-mail');</script>";
        }else if($quantidadeusuario >= 1){
            echo "<script>alert('Este Usuário já encontra-se cadastrado na base de dados! Favor informe outro Usuário');</script>";
        }else{
        
            $sqlendereco = "UPDATE endereco 
                                SET bairro = '$bairro', 
                                    rua = '$rua',
                                    numero = $numero,
                                    cep = '$cep', 
                                    pontoreferencia = '$pontoreferencia',
                                    complemento = '$complemento',
                                    ativo = '$ativo',
                                    observacao = '$observacaoendereco',
                                    idcidade = $idcidade 
                              WHERE id = $id";
            unset($dados['id']);
            unset($dados['nomefantasia']);
            unset($dados['razaosocial']);
            unset($dados['email']);
            unset($dados['cnpj']);
            unset($dados['inscricaoestadual']);
            unset($dados['dataabertura']);
            unset($dados['telefone']);
            unset($dados['celular']);
            unset($dados['usuario']);
            unset($dados['senha']);
            unset($dados['ativo']); 
            unset($dados['observacao']);
            unset($dados['idcidade']);
            unset($dados['bairro']);
            unset($dados['rua']);
            unset($dados['numero']);
            unset($dados['cep']);
            unset($dados['pontoreferencia']);
            unset($dados['complemento']);
            unset($dados['observacaoendereco']);
            $queryendereco = $this->bd->prepare($sqlendereco);
            if($queryendereco->execute($dados) == 1){
                $sqlpessoa = "UPDATE pessoa 
                                SET nome = '$nome',
                                    telefone = '$telefone', 
                                    celular = '$celular',
                                    observacao = '$observacao',
                                    email = '$email',
                                    usuario = '$usuario',
                                    senha = '$senha',
                                    idendereco = $id
                              WHERE idendereco = $id";
                $querypessoa = $this->bd->prepare($sqlpessoa);
                if($querypessoa->execute($dados) == 1){
                    $verificaregistropessoa = "select p.id as idpessoa 
                                               from pessoa p 
                                              inner join pessoajuridica pj 
                                                 on p.id = pj.idpessoa 
                                              where p.idendereco = $id";
                    $sqlverificaregistropessoa = $this->bd->prepare($verificaregistropessoa);
                    if($sqlverificaregistropessoa->execute() == 1){
                        if ($sqlverificaregistropessoa->rowCount() > 0) {
                            foreach ($sqlverificaregistropessoa as $rs) {   
                                $verificaidregistropessoa = $rs["idpessoa"];
                            }
                        }

                        $sqlpessoajuridica = "UPDATE pessoajuridica 
                                               SET cnpj = '$cnpj', 
                                                   razaosocial = '$razaosocial',
                                                   inscricaoestadual = '$inscricaoestadual',
                                                   dataabertura = '$dataabertura'
                                             WHERE idpessoa = $verificaidregistropessoa";
                        $querypessoajuridica = $this->bd->prepare($sqlpessoajuridica);
                        return $querypessoajuridica->execute($dados);
                    }
                }
            }
        }
    }

    public function excluir($iddecode) {
        //Consultar id da pessoa fisica
        $sqlconsultapessoajuridica = "select pj.id as idpessoajuridica
                                      from endereco ende
                                     inner join pessoa pes 
                                        on ende.id = pes.idendereco
                                     inner join pessoajuridica pj
                                        on pes.id = pj.idpessoa
                                     where ende.id = $iddecode";
        $sqlverificaidpessoajuridica = $this->bd->prepare($sqlconsultapessoajuridica);
        $sqlverificaidpessoajuridica->execute();
        if ($sqlverificaidpessoajuridica->rowCount() > 0) {
            foreach ($sqlverificaidpessoajuridica as $rs) {   
                $idpessoajuridica = $rs["idpessoajuridica"];
            }
        }
        
        //Consultar se a pessoajuridica nao possui nenhum agendamento
        $sqlconsultaagendamentopessoajuridica = "select count(*) as existe
                                                    from endereco ende
                                                   inner join pessoa pes 
                                                      on ende.id = pes.idendereco
                                                   inner join pessoajuridica pj
                                                      on pes.id = pj.idpessoa
                                                   inner join agendamento agen
                                                      on pes.id = agen.idpessoa
                                                   where ende.id = $iddecode";
        
        $sqlverificaagendamentopessoajuridica = $this->bd->prepare($sqlconsultaagendamentopessoajuridica);
        $sqlverificaagendamentopessoajuridica->execute();
        if ($sqlverificaagendamentopessoajuridica->rowCount() > 0) {
            foreach ($sqlverificaagendamentopessoajuridica as $rs) {   
                $existeagendamento = $rs["existe"];
            }
        }
        
        if($existeagendamento == 0){
            $pessoajuridica    = "delete from pessoajuridica where id = $idpessoajuridica";
            $sqlpessoajuridica = $this->bd->prepare($pessoajuridica);
            $this->bd->prepare($pessoajuridica);
            if($sqlpessoajuridica->execute() == 1){
                $pessoa    = "delete from pessoa where idendereco = $iddecode";
                $sqlpessoa = $this->bd->prepare($pessoa);
                $this->bd->prepare($pessoa);
                if($sqlpessoa->execute() == 1){
                    $endereco    = "delete from endereco where id = $iddecode";
                    $sqlendereco = $this->bd->prepare($endereco);
                    $this->bd->prepare($endereco);
                    return $sqlendereco->execute();
                }
            }
        }else{
            //Retorna 0 == false
            return 0;
        }
        
    }

}
