<?php

class CidadeModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $sql = "INSERT INTO cidade(nome, idestado) VALUES(:nome, :idestado)";
        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "select ci.id,
                       ci.nome as nomecidade,
                       es.nome as nomeestado,
                       es.uf as uf
                  from cidade ci 
                 inner join estado es 
                    on ci.idestado = es.id 
                 order by ci.nome asc;";
        
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function buscar($iddecode) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = "SELECT ci.id, 
                       ci.nome as nomecidade, 
                       ci.idestado,
                       es.uf
                  FROM cidade ci
                 INNER JOIN estado es
                    ON ci.idestado = es.id
                 WHERE ci.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $iddecode));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        $id = $_POST['id'];
        
        $sql = "UPDATE cidade 
                   SET nome = :nome, 
                       idestado = :idestado
                 WHERE id = $id";

        unset($dados['id']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function excluir($iddecode) {
        $sql = "DELETE FROM cidade WHERE id = :id";
        $query = $this->bd->prepare($sql);
        return $query->execute(array('id' => $iddecode));
    }

}
