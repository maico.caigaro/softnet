<?php

class ContratoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $datacontrato = $_POST['datacontrato'];
        $conectadonarede = $_POST['conectadonarede'];
        $idagendamento = $_POST['idagendamento'];
        $observacao = $_POST['observacao'];
        
        if(isset($_POST['datacancelamento'])){
            $datacancelamento ='null';
        }else{
            $datacancelamento = $_POST['datacancelamento'];
        }
        if(isset($_POST['valormulta'])){
            $valormulta =0.00;
        }else{
            //Input Valor
            //Input Valor Multa
            $inputvalormulta      = str_replace("R$", "", str_replace("", "", $dados['valormulta']));
            $formatainputmulta = str_replace(".", "", str_replace("", "", $inputvalormulta));
            $formatavalormulta = str_replace(",", ".", str_replace("", "", $formatainputmulta));
            $valormulta   = $formatavalormulta;
        }
        if(isset($_POST['motivocancelamento'])){
            $motivocancelamento = "";
        }else{
            $motivocancelamento = $_POST['motivocancelamento'];
        }
        
        $sql = "INSERT INTO contrato(datacontrato, 
                                     datacancelamento,
                                     motivocancelamento,
                                     observacao,
                                     conectadonarede,
                                     valormulta,
                                     idagendamento) 
                              VALUES('$datacontrato', 
                                     $datacancelamento,
                                     '$motivocancelamento',
                                     '$observacao',
                                     '$conectadonarede',
                                     $valormulta,
                                     $idagendamento)";
        unset($dados['id']);
        unset($dados['datacontrato']);
        unset($dados['idagendamento']);
        unset($dados['datacancelamento']);
        unset($dados['motivocancelamento']);
        unset($dados['observacao']);
        unset($dados['conectadonarede']);
        unset($dados['valormulta']);
        unset($dados['situacaocontrato']);
        unset($dados['datavencimento']);
        $query = $this->bd->prepare($sql);
        if($query->execute($dados) == 1){
            //Quando entrar aqui a programação irá inserir as parcelas na tabela boleto
            //Geração de Boleto
            $idagendamento = $_POST['idagendamento'];

            //Consulta o último id do contrato
            $consultaultimoidcontrato = "select max(con.id) as idcontrato
                                           from contrato con";
            $consultaultimocontrato = $this->bd->prepare($consultaultimoidcontrato);
            $consultaultimocontrato->execute();

            if ($consultaultimocontrato->rowCount() > 0) {
                foreach ($consultaultimocontrato as $rs) {
                    $idcontrato = $rs["idcontrato"];
                }
            }

            //Consulta o id da pessoa pelo id do agendamento, idtiposervico
            $consultaidpessoa = "select pe.id as idpessoa,
                                        agen.idtiposervico as idtiposervico,
                                        ser.valorplano as valorservico
                                   from agendamento agen
                                  inner join endereco ende
                                     on agen.idendereco = ende.id
                                  inner join pessoa pe
                                     on pe.idendereco = ende.id
                                  inner join servico ser
                                     on agen.idservico = ser.id
                                  where agen.id = $idagendamento";
            $consultapessoaid = $this->bd->prepare($consultaidpessoa);
            $consultapessoaid->execute();

            if ($consultapessoaid->rowCount() > 0) {
                foreach ($consultapessoaid as $rs) {
                    $idpessoa = $rs["idpessoa"];
                    $idtiposervico = $rs["idtiposervico"];
                    $valorservico = $rs["valorservico"];
                }
            }
            //Será gerada 12 parcelas para o boleto
            $quantidadeparcelasboleto = 12;
            
            $data = $_POST['datavencimento'];
            for($i=1;$i<=$quantidadeparcelasboleto;$i++){
                $data = explode("/", $data);
                $data_vencimento = date("d/m/Y", mktime(0,0,0,$data[1]+1,$data[0],$data[2]));
               
                $sql_contrato = 
                    "INSERT INTO boleto (
                      valor, 
                      datavencimento, 
                      idpessoa, 
                      idtiposervico , 
                      idcontrato)
                    VALUES(
                      $valorservico,
                      '$data_vencimento',
                      $idpessoa,
                      $idtiposervico,
                      $idcontrato
                    );";
                $data = $data_vencimento;
                $query_insere_boleto = $this->bd->prepare($sql_contrato);
                $query_insere_boleto->execute();
            }
            //Este retorno é para indicar que o registro está sendo inserido caso não haja nenhuma falha
            return 1;
        }else{
            //Será excluido o último contrato inserido
            //Primeiro executa a consulta do contrato para buscar o maximo id que quer dizer o ultimo id inserido
            $consultaultimoidcontrato = "select max(id) as idcontrato
                                           from contrato";
            $consultaultimocontratoid = $this->bd->prepare($consultaultimoidcontrato);
            $consultaultimocontratoid->execute();

            if ($consultaultimocontratoid->rowCount() > 0) {
                foreach ($consultaultimocontratoid as $rs) {
                    $idultimocontrato = $rs["idcontrato"];
                }
            }
            
            //Exclusao do ultimo idcontrato inserido
            $exclusaocontrato = "delete from contrato where id = $idultimocontrato";
            $prepara_exclusaocontrato = $this->bd->prepare($exclusaocontrato);
            $this->bd->prepare($prepara_exclusaocontrato);
            return $prepara_exclusaocontrato->execute();
        }        
    }

    public function buscarTodos() {
        //$tipopessoa = $_POST['tipopessoa'];
        $tipopessoa = 'F';
        if($tipopessoa == 'F'){
            if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }


            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
            $sqlpessoafisica = "select con.id,
                                        to_char(con.datacontrato, 'dd/MM/yyyy') as datacontrato,
                                        'R$ ' || LTRIM(to_char(con.valormulta, '9G999G990D99')) as valormulta,
                                        to_char(con.datacancelamento, 'dd/MM/yyyy') as datacancelamento,
                                        pes.nome as nomepessoa,
                                        pes.telefone,
                                        pes.celular,
                                        pes.email,
                                        con.conectadonarede,
                                        'R$ ' || LTRIM(to_char(serv.valorplano, '9G999G990D99')) as valorplano,
                                        tipo.descricao as descricaoservico
                                   from contrato con
                                  inner join agendamento agend
                                     on con.idagendamento = agend.id
                                  inner join tiposervicos tipo
                                     on agend.idtiposervico = tipo.id
                                  inner join endereco ende
                                     on agend.idendereco = ende.id
                                  inner join pessoa pes
                                     on pes.idendereco = ende.id 
                                  inner join servico serv
                                     on serv.id = agend.idservico
                                   left join pessoafisica pf
                                     on pf.idpessoa = pes.id
                                   left join pessoajuridica pj
                                     on pj.idpessoa = pes.id
                                  order by con.conectadonarede desc; ";
            $querypessoafisica = $this->bd->query($sqlpessoafisica);
            return $querypessoafisica->fetchAll();
        }
    }

    public function buscar($iddecode) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        
        $sql = " select con.id,
                        con.observacao,
                        to_char(con.datacontrato, 'dd/MM/yyyy') as datacontrato,
                        to_char(con.datacancelamento, 'dd/MM/yyyy') as datacancelamento,
                        con.motivocancelamento,
                        con.conectadonarede,
                        'R$ ' || LTRIM(to_char(con.valormulta, '9G999G990D99')) as valorplano,
                        con.idagendamento
                   from contrato con
                  inner join agendamento agend
                     on con.idagendamento = agend.id
                  inner join tiposervicos tipo
                     on agend.idtiposervico = tipo.id
                  inner join endereco ende
                     on agend.idendereco = ende.id
                  inner join pessoa pes
                     on pes.idendereco = ende.id 
                  inner join servico serv
                     on serv.id = agend.idservico
                   left join pessoafisica pf
                     on pf.idpessoa = pes.id
                   left join pessoajuridica pj
                     on pj.idpessoa = pes.id
                  where con.id = :id";
        $query = $this->bd->prepare($sql);
        $query->execute(array('id' => $iddecode));

        return $query->fetch();
    }

    public function atualizar(array $dados) {
        //Valor formatação
        $formatavalormulta  = str_replace("R$", "", str_replace("", "", $dados['valormulta']));
        
        //Valor formatação
        $valormulta = $formatavalormulta;
        $verificavalormulta = substr($valormulta, -3, 1);
        if($verificavalormulta == ","){
            $inputformatamulta      = str_replace("R$", "", str_replace("", "", $valormulta));
            $formatavalormulta = str_replace(".", "", str_replace("", "", $inputformatamulta));
            $valorformatadomulta = str_replace(",", ".", str_replace("", "", $formatavalormulta));
            $valorformatadofinalmulta   = $valorformatadomulta;
        }else{
            $valormulta = $dados['valormulta'];
            $formatamulta      = str_replace("R$", "", str_replace("", "", $valormulta));
            $removevirgulamulta = str_replace(",", "", $formatamulta);
            $valorformatadofinalmulta = $removevirgulamulta;
        }
        
        if($_POST['datacancelamento'] == ''){
            $conectadonarede = 'S';
        }else{
            $conectadonarede = 'N';
        }
        $id = $_POST['id'];
        $sql = "UPDATE contrato 
                   SET datacontrato       = :datacontrato,
                       datacancelamento   = :datacancelamento,
                       motivocancelamento = :motivocancelamento,
                       observacao         = :observacao,
                       conectadonarede    = '$conectadonarede',
                       valormulta         = '$valorformatadofinalmulta',
                       idagendamento      = :idagendamento
                 WHERE id = $id";
        unset($dados['id']);
        unset($dados['valormulta']);
        unset($dados['situacaocontrato']);
        unset($dados['conectadonarede']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }
}
