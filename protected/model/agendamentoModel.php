<?php

class AgendamentoModel extends Conexao {

    function __construct() {
        parent::__construct();
    }

    public function inserir(array $dados) {
        $idendereco = $_POST['idendereco'];
        
        $consultaidpessoa = "select pe.id as idpessoa
                               from pessoa pe
                               inner join endereco ende
                                  on pe.idendereco = ende.id
                               where ende.id = $idendereco";
        $consultapessoaid = $this->bd->prepare($consultaidpessoa);
        $consultapessoaid->execute();

        if ($consultapessoaid->rowCount() > 0) {
            foreach ($consultapessoaid as $rs) {
                $idpessoa = $rs["idpessoa"];
            }
        }
 
        $sql = "INSERT INTO agendamento(descricao, dataabertura, turnoinstalado, idtiposervico, idendereco, status, idservico, idpessoa) "
                . "VALUES(:descricao, :dataabertura, :turnoinstalacao, :idtiposervico,  $idendereco, :status, :idservico, $idpessoa)";

        unset($dados['id']);
        unset($dados['idpessoa']);
        unset($dados['idendereco']);
        $query = $this->bd->prepare($sql);
        return $query->execute($dados);
    }

    public function buscarTodos() {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "select pes.nome as nomepessoa,
                        pes.telefone,
                        pes.email,
                        ende.bairro,
                        ende.rua,
                        (cid.nome || ' - ' || esta.uf) as nomecidade,
                        to_char(ag.dataabertura, 'dd/MM/yyyy') as dataabertura,
                        ag.id,
                        ag.status as status,
                        tipo.descricao as descricaoservico,
                        (pes.nome || ' - ' || tipo.descricao) as clienteservico,
                        (ser.tipoplano || ' - ' || ser.tipoplano) as descricaocompletaservico,
                        ('R$ ' || ser.valorplano) as valorplano
               from agendamento ag
               left join servico ser
                 on ag.idservico = ser.id
               left join tiposervicos tipo
                 on ag.idtiposervico = tipo.id
              left join endereco ende
                 on ag.idendereco = ende.id
              left join pessoa pes
                 on ende.id = pes.idendereco
              left join pessoafisica pj
                 on pes.id = pj.idpessoa
              left join cidade cid
                 on ende.idcidade = cid.id
              left join estado esta
                 on cid.idestado = esta.id
              order by dataabertura, status, nomepessoa asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }
    
    public function buscarAgendamentoPessoa($idenderecopessoabusca) {
        if( isset($_SERVER['HTTPS'] ) ) {
            $prefixo = 'https://';
        }else{
            $prefixo = 'http://';
        }
        
        
        $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST']. '/';
        $sql = "select tipo.descricao as descricaoservico,
                       ag.descricao as descricaoagenda,
                       to_char(ag.dataabertura, 'dd/MM/yyyy') as dataabertura,
                       ag.turnoinstalado,
                       ag.status,
                       (ser.tipoplano || ' - ' || ser.tipoplano) as descricaocompletaservico,
                       ('R$ ' || ser.valorplano) as valorplano
                  from pessoa pe
                 inner join endereco en
                    on pe.idendereco = en.id
                 inner join agendamento ag
                    on ag.idendereco = en.id
                 inner join servico ser
                    on ag.idservico = ser.id   
                 inner join tiposervicos tipo
                    on ag.idtiposervico = tipo.id
                 where pe.idendereco = $idenderecopessoabusca
                 order by ag.dataabertura, ag.status asc;";
        $query = $this->bd->query($sql);
        return $query->fetchAll();
    }

    public function finalizaragendamento($iddecode) {
        $finalizaagendamento = "UPDATE agendamento 
                                   SET status = 'F'
                                 WHERE id = $iddecode";
        $finaliza = $this->bd->prepare($finalizaagendamento);
        if($finaliza->execute() == 1){
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "insertsucess";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=agendamentoController&acao=listar&insert=$encriptografa'</script>";
        }else{
            //Redirecionar
            if(isset($_SERVER['HTTPS'] ) ) {
                $prefixo = 'https://';
            }else{
                $prefixo = 'http://';
            }

            $urlbase = $prefixo . ''. $_SERVER['HTTP_HOST'];
            //codificarget
            $string = "inserterror";
            $encriptografa = base64_encode($string);
            echo "<script>window.location.href='$urlbase/softnet/index.php?controle=agendamentoController&acao=listar&inserterror=$encriptografa'</script>";
        }
    }
}
