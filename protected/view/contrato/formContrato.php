<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cadastro de Contrato</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form action="<?php echo $acao; ?>" name="formContrato" id="formContrato" method="POST" role="form" data-parsley-validate class="form-horizontal form-label-left">
                        <?php
                            if(isset($_GET['id']) != ''){
                                $alterando = 1;
                            }else{
                                $alterando = 0;
                            }
                            $datacontrato = date ("d/m/Y");
                        ?>
                        <input type="hidden" name="id" value="<?php if (isset($contrato)) echo $contrato['id']; ?>"/>
                        <input type="hidden" name="datacontrato" value="<?php echo $datacontrato; ?>"/>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="situacao">Situação do Contrato<span class="required">*</span></label>
                        
                        <script>
                            $(document).ready(function(){
                                $("#datacancel").hide();
                                $("#valormultacancel").hide();
                                $("#motivocancel").hide();

                                $("#situacaocontrato").change(function() {
                                    loadSituation();
                                });

                                loadSituation();
                            });

                            function loadSituation(){
                                var val = $("#situacaocontrato").val();
                                if(val==0){
                                    $("#datacancel").fadeOut();
                                    $("#valormultacancel").fadeOut();
                                    $("#motivocancel").fadeOut();
                                }else{
                                    $("#datacancel").fadeIn();
                                    $("#valormultacancel").fadeIn();
                                    $("#motivocancel").fadeIn();
                                }
                            }
                        </script>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="select2_single form-control" name="situacaocontrato" id="situacaocontrato" required data-errormessage-value-missing="Selecione a situação do Contrato">
                                    <?php if($alterando == 1){?>
                                        <option value="0" selected >Novo Contrato</option>
                                        <option value="1"  >Cancelamento de Contrato</option>
                                    <?php }else{ ?>
                                        <option value="0" selected >Novo Contrato</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Cliente Agendado</label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                              <select class="select2_single form-control" name="idagendamento" id="idagendamento" required data-errormessage-value-missing="Selecione o Agendamento">
                                  <option value="">Selecione o Cliente Agendado</option>
                                <?php
                                foreach ($listaAgendamentos as $agendamentos) {
                                    $selected = (isset($contrato) && $contrato['idagendamento'] == $agendamentos['id']) ? 'selected' : '';
                                    ?>
                                    <option value='<?php echo $agendamentos['id']; ?>'
                                            <?php echo $selected; ?>> 
                                                <?php echo $agendamentos['clienteservico']; ?>
                                    </option>
                                <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datacontrato">Data Contrato<span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-8">
                                <input type="text" id="datacontrato" name="datacontrato" placeholder="Informe a Data Contrato"
                                       autocomplete="off" maxlength="100" value="<?php echo $datacontrato; ?>" disabled="" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group" id="datacancel">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datacancelamneto">Data Cancelamento
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-8">
                                <input type="text" id="datacancelamento" name="datacancelamento" placeholder="Informe a Data de Cancelamento"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($contrato)) echo $contrato['datacancelamento']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="conectadonarede">Conectado na Rede <span class="required">*</span>
                            </label>
                        <div class="form-group" style="margin-left: 19%;">
                            <label class="col-md-2"><div class="radio" id="conectadonarede"><span><input type="radio" name="conectadonarede" value="S" checked=""
                              <?php if(isset($contrato)) if ($contrato['conectadonarede'] == 'S'){echo 'checked';}else{$contrato == null;} ?>>Conectado</span></div></label>
                              <label><div class="radio" id="conectadonarede"><span><input type="radio" name="conectadonarede" value="N"
                              <?php if(isset($contrato)) if ($contrato['conectadonarede'] == 'N'){echo 'checked';}else{$contrato == null;} ?>>Não Conectado</span></div></label>
                            
                        </div>
                        <div class="form-group" id="valormultacancel">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="valormulta">Valor Multa <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-8">
                                <input type="text" id="valormulta" name="valormulta" placeholder="Valor Multa"
                                       autocomplete="off" value="<?php if (isset($servico)) echo $servico['valormulta']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group" id="motivocancel">
                            <label class="control-label col-md-2 col-sm-2 col-xs-10">Motivo Cancelamento</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <textarea class="form-control" rows="3" id="motivocancelamento" name="motivocancelamento" maxlength="200" placeholder="Informe o Motivo de Cancelamento"><?php if (isset($contrato)) echo $contrato['motivocancelamento']; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-10">Observação</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <textarea class="form-control" rows="3" id="observacao" name="observacao" maxlength="2000" placeholder="Informe a Observação"><?php if (isset($contrato)) echo $contrato['observacao']; ?></textarea>
                            </div>
                        </div>
                        
                        <?php
                            if($alterando != 1){?>
                                <div class="form-group">
                                    <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datapagamento">Data Vencimento <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                        <input type="text" id="datavencimento" name="datavencimento" autocomplete="off" value="<?php if (isset($contrato)) echo $contrato['datavencimento']; ?>" class="form-control" placeholder="Informe a Data de Pagamento">
                                    </div>
                                </div>
                            <?php }
                        ?>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                              <input type="submit" class="btn btn-success" value="Gravar" />
                              <?php
                                if($alterando == 1){?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-primary disabled" type="reset" value="Limpar" />
                                <?php } else {?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-info" type="reset" value="Limpar" />
                                <?php }
                              ?>
                          </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="src/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $("#formContrato").validate({
        rules: {
            idagendamento: {
                required: true
            },
            datacancelamento: {
                required: true
            },
            valormulta: {
                required: true
            },
            datapagamento: {
                required: true
            },
            motivocancelamento: {
                required: true,
                minlength: 10,
                maxlength: 200
            }
        },
        messages: {
            idagendamento: {
                required: "Por favor, selecione um Cliente Agendado"
            },
            datacancelamento: {
                required: "Por favor, informe a Data de Cancelamento"
            },
            valormulta: {
                required: "Por favor, informe o Valor de Multa"
            },
            datapagamento: {
                required: "Por favor, informe a Data de Pagamento"
            },
            motivocancelamento: {
                required: "Por favor, informe o Motivo de Cancelamento",
                minlength: "O Motivo de Cancelamento deve ter pelo menos 10 caracteres",
                maxlength: "O Motivo de Cancelamento deve ter no máximo 200 caracteres"
            }
        }
    });
</script>