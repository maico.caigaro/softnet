<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <?php
        if(isset($_GET['updt']) == 'dXBkYXRlc3RhdHVz'){?>
            <div class="alert alert-success">
                Dados atualizados com <strong>Sucesso</strong>
            </div>
        <?php }else if(isset($_GET['updterror']) == 'dXBkYXRlZXJybw=='){?>
            <div class="alert alert-danger">
                Não foi possível atualizar o registro.
            </div>
        <?php }else if(isset($_GET['dlt']) == 'ZGVsZXRlc3VjZXNz'){?>
            <div class="alert alert-success">
                Registro Removido com <strong>Sucesso</strong>
            </div>
        <?php }else if(isset($_GET['dlterror']) == 'ZGVsZXRlZXJyb3I='){?>
            <div class="alert alert-danger">
                Não foi possível excluir o registro.
            </div>
        <?php }else if(isset($_GET['insert']) == 'aW5zZXJ0c3VjZXNz'){?>
            <div class="alert alert-success">
                Registro inserido com <strong>Sucesso</strong>
            </div>
        <?php }else if(isset($_GET['inserterror']) == 'aW5zZXJ0ZXJyb3I='){?>
            <div class="alert alert-danger">
                Não foi possível inserir o registro.
            </div>
        <?php }
    ?>
        <div class="x_panel">
            <div class="x_title">
                <h2>Relação de Contratos</h2>
                <div class="clearfix"></div>
            </div>
            <a href="index.php?controle=contratoController&acao=novo">
                <span class='glyphicon glyphicon-plus' title="Adicionar Contrato"> Adicionar</span>
            </a>
            <br/><br/>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th style="width: 200px;">Cliente</th>
                            <th style="text-align: center">Data Contrato</th>
                            <th style="width: 200px;">Tipo Serviço</th>
                            <th style="text-align: center; width: 100px">Valor Plano</th>
                            <th style="width: 120px;">Contato</th>
                            <th style="width: 120px;">E-mail</th>
                            <th style="text-align: center; width: 100px">Valor Multa</th>
                            <th style="text-align: center">Situação Contrato</th>
                            <th style="text-align: center;">Gerar Contrato</th>
                        </tr>
                    </thead>

                    <tbody> 
                        <?php
                            foreach ($listaDados as $item) {
                                echo '<td>' . $item['nomepessoa'];
                                echo '<td style="text-align: center">' . $item['datacontrato'];
                                echo '<td>' . $item['descricaoservico'];
                                echo '<td style="text-align: center">' . $item['valorplano'];
                                if($item['telefone'] == ""){
                                    $contato = $item['celular'];
                                }else if($item['celular'] == ""){
                                    $contato = $item['telefone'];
                                }else if($item['telefone'] != "" && $item['celular'] != ""){
                                    $contato = $item['telefone'];
                                }else{
                                    $contato = "Nenhum contato informado";
                                }
                                echo '<td>' . $contato;
                                echo '<td>' . $item['email'];
                                echo '<td style="text-align: center">' . $item['valormulta'];
                                $id = base64_encode($item['id']);
                                $conectadonarede = base64_encode($item['conectadonarede']);
                                if($item['datacancelamento'] != ''){
                                    echo "<td style='text-align: center;'> <a href='' title='Contrato Cancelado' class='btn btn-danger btn-xs'><i class=''></i> Cancelado </a>";
                                    echo '<td style="text-align: center; background-color: mistyrose;">' . "<img src='http://localhost/softnet/protected/imagens/comprovante.png' style='width: 28px;' title='Contrato Cancelado'/>";
                                }else{
                                    echo "<td style='text-align: center;'> <a href='index.php?controle=contratoController&acao=buscar&id=$id' title='Clique aqui para alterar o registro'"
                                        . " class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Alterar </a>"
                                . "</a> </td>";
                                    echo '<td style="text-align: center">' . "<a href='protected/view/contrato/rel_contrato.php?id=$id'> <img src='http://localhost/softnet/protected/imagens/comprovante.png' style='width: 28px;'/></a>";
                                }
                                echo '</tr>';
                            }
                        ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>