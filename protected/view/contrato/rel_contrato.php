<?php
include '../../../config/confloginrel.php';
require_once("../../fpdf/fpdf.php");

//P = Paisagem
//L = Retrato
$pdf = new FPDF("L", "pt", "A4");

$pdf->AddPage('P');
$pdf->Ln(3);

//Esse valor vai vir através do GET
$idcontrato =base64_decode($_GET['id']);

//Contorno Margens do relatório
$pdf->Line(585, 10, 10, 10);
$pdf->Line(10, 825, 10, 10);
$pdf->Line(585, 825, 585, 10);
$pdf->Line(585, 825, 10, 825);

//Imagem do logo Softnet
$pdf->Image('../../imagens/logo_softnet.png', +20,30,110);


$pdf->SetFont('Arial', 'U', 10);

$pdf->Ln();$pdf->Ln();

$pdf->Cell(630, 7, utf8_decode('TERMO DE CONTRATAÇÃO DOS SERVIÇOS DE CONEXÃO À INTERNET E'), 0, 0, 'C');
$pdf->Ln();$pdf->Ln();
$pdf->Cell(630, 7, utf8_decode('SERVIÇOS DE COMUNICAÇÃO MULTIMÍDIA Nº 000000'), 0, 0, 'C');

$pdf->Ln();$pdf->Ln();$pdf->Ln();$pdf->Ln();
//Cabeçalho referente a Qaulificiação Contratada
$pdf->SetFont('Arial', 'B', 10);
$pdf->Ln(20);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(565, 14, utf8_decode('QUALIFICAÇÃO DA CONTRATADA:'), 1, 1, 'L', 10);

//Cabeçalho referente as informações do cliente
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(120, 15, utf8_decode('Razão Social:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(250, 15, utf8_decode('Softnet Ltda - CNPJ:'), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(60, 15, utf8_decode('CNPJ: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(135, 15, ltrim(utf8_decode('60.607.649/0001-05')), 1, 1, 'L', 10);

//Linha referente ao Endereço
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(90, 15, utf8_decode('Endereço:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(250, 15, utf8_decode('Rua General Canabarro, 50 Sala 01'), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(60, 15, utf8_decode('Bairro: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(165, 15, ltrim(utf8_decode('Centro')), 1, 1, 'L', 10);

//Linha referente ao Endereço
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(95, 15, utf8_decode('Complemento:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(140, 15, utf8_decode('Perto do Prédio Azul'), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(60, 15, utf8_decode('Cidade: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(125, 15, ltrim(utf8_decode('Passo Fundo')), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(30, 15, utf8_decode('UF: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(30, 15, ltrim(utf8_decode('RS')), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(30, 15, utf8_decode('CEP: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(55, 15, ltrim(utf8_decode('98300-000')), 1, 1, 'L', 10);

//Linha referente a Contato
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(90, 15, utf8_decode('Telefone:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(80, 15, utf8_decode('54 3241-0000'), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(60, 15, utf8_decode('Site: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(130, 15, ltrim(utf8_decode('www.softnet.com.br')), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(50, 15, utf8_decode('e-mail: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(155, 15, ltrim(utf8_decode('faleconosco@softnet.com.br')), 1, 1, 'L', 10);

//Linha referente a Anatel
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(120, 15, utf8_decode('Autorização na ANATEL:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(445, 15, utf8_decode('ATO n.º 109/2008 de 11/01/2009 - Termo de Autorização de n.º 011/2008 - PROCESSO n.º 53500.020007/2007.'), 1, 0, 'L', 10);

$pdf->Ln(10);

//Cabeçalho referente a qualificiação do cliente contratante
$pdf->SetFont('Arial', 'B', 10);
$pdf->Ln(20);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(565, 14, utf8_decode('QUALIFICAÇÃO DO CLIENTE (CONTRATANTE):'), 1, 1, 'L', 10);

//SQL VERIFICA SE O ID DO QUE ESTA VINDO É DA PESSOA FISICA OU JURIDICA
$query = "select pf.id as idpessoafisica,
                 pj.id as idpessoajuridica
            from contrato con
            left join agendamento agen
              on con.idagendamento = agen.id
            left join endereco ende
              on agen.idendereco = ende.id
            left join pessoa pe
              on ende.id = pe.idendereco
            left join pessoafisica pf
              on pe.id = pf.idpessoa
            left join pessoajuridica pj
              on pe.id = pj.idpessoa
           where con.id = $idcontrato";
    $result = pg_query($query);
    
 while ($consulta = pg_fetch_assoc($result)) {
        if($consulta["idpessoafisica"] != ''){
            $consultacolunas = "pf.cpf,
                                pf.rg,
                                to_char(pf.datanascimento, 'dd/MM/yyyy') as datanascimento,";
            $filtropessoa = "inner join pessoafisica pf on pe.id = pf.idpessoa";
        }else{
            $consultacolunas = "pj.cnpj,
                                pj.razaosocial,
                                pj.inscricaoestadual,
                                to_char(pj.dataabertura, 'dd/MM/yyyy') as dataabertura,";
            $filtropessoa = "inner join pessoajuridica pj on pe.id = pj.idpessoa";
        }
        
 }
 
 //SQL VERIFICA SE O ID DO QUE ESTA VINDO É DA PESSOA FISICA OU JURIDICA
$querypessoa = "select $consultacolunas
                        ende.bairro,
                        ende.rua,
                        ende.numero,
                        ende.cep,
                        ende.pontoreferencia,
                        ende.complemento,
                        cid.nome as nomecidade,
                        est.nome as nomeestado,
                        est.uf,
                        pe.nome as nomepessoa,
                        pe.telefone,
                        pe.celular,
                        pe.email,
                        ser.tipoplano,
                        ser.velocidadeplano,
                        'R$ ' || LTRIM(to_char(ser.valorplano, '9G999G990D99')) as valorplano
                   from contrato as con
                   join agendamento agen
                     on con.idagendamento = agen.id 
                  inner join endereco ende
                     on agen.idendereco = ende.id
                  inner join cidade cid
                     on ende.idcidade = cid.id
                  inner join estado est
                     on cid.idestado = est.id
                  inner join servico ser
                     on agen.idservico = ser.id
                  inner join pessoa pe
                     on pe.idendereco = ende.id
                        $filtropessoa
                  where con.id = $idcontrato";
    $resultpessoa = pg_query($querypessoa);
    
 while ($consulta = pg_fetch_assoc($resultpessoa)) {
     $tipoplano = $consulta["tipoplano"];
     $velocidadeplano = $consulta["velocidadeplano"];
     $valorplano = $consulta["valorplano"];
     $bairro = $consulta["bairro"];
     $rua = $consulta["rua"];
     $numero = $consulta["numero"];
     $cep = $consulta["cep"];
     $pontoreferencia = $consulta["pontoreferencia"];
     $complemento = $consulta["complemento"];
     $nomecidade = $consulta["nomecidade"];
     $nomeestado = $consulta["nomeestado"];
     $uf = $consulta["uf"];
     $nomepessoa = $consulta["nomepessoa"];
     $telefone = $consulta["telefone"];
     $celular = $consulta["celular"];
     $email = $consulta["email"];
     //Fazer os testes com as colunas, cpf, rg, cnpj, razaosocial, inscricaoestadual
     if(isset($consulta["cpf"]) != "" && isset($consulta["rg"]) != ""){
         $cpf = $consulta["cpf"];
         $rg = $consulta["rg"];
         $datanascimento = $consulta["datanascimento"];
         $cpfcnpj = $consulta["cpf"];
         $dadostipopessoa = 0;
     }else{
         $cnpj = $consulta["cnpj"];
         $razaosocial = $consulta["razaosocial"];
         $inscricaoestadual = $consulta["inscricaoestadual"];
         $dataabertura = $consulta["dataabertura"];
         $cpfcnpj = $consulta["cnpj"];
         $dadostipopessoa = 1;
     }
 }
 
//Cabeçalho referente as informações do contratante
 if($dadostipopessoa == 1){
     $pdf->SetFont('Arial', '', 10);
     $pdf->Cell(80);
     $pdf->setFillColor(230,230,230);
     $pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
     $pdf->Cell(120, 15, utf8_decode('Razão Social/Nome:'), 1, 0, 'L', 10);
     $pdf->setFillColor(255,255,255);
     $pdf->Cell(250, 15, utf8_decode($razaosocial), 1, 0, 'L', 10);
     $pdf->setFillColor(230,230,230);
     $pdf->Cell(60, 15, utf8_decode('CNPJ/CPF: '), 1, 0, 'C', 10);
     $pdf->setFillColor(255,255,255);
     $pdf->Cell(135, 15, ltrim(utf8_decode($cnpj)), 1, 1, 'L', 10);
     $pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
     
     $pdf->setFillColor(230,230,230);
     $pdf->Cell(100, 15, utf8_decode('Responsável '), 1, 0, 'L', 10);
     $pdf->setFillColor(255,255,255);
     $pdf->Cell(130, 15, ltrim(utf8_decode($nomepessoa)), 1, 0, 'L', 10);
     $pdf->setFillColor(230,230,230);
     $pdf->Cell(50, 15, utf8_decode('E-mail '), 1, 0, 'L', 10);
     $pdf->setFillColor(255,255,255);
     $pdf->Cell(285, 15, ltrim(utf8_decode($email)), 1, 1, 'L', 10);
 }


//Linha referente a RG, Responsável
 if($dadostipopessoa == 0){
     $pdf->SetFont('Arial', '', 10);
     $pdf->Cell(80);
     $pdf->setFillColor(230,230,230);
     $pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
     $pdf->Cell(50, 15, utf8_decode('RG:'), 1, 0, 'L', 10);

     $pdf->setFillColor(255,255,255);
     $pdf->Cell(70, 15, utf8_decode($rg), 1, 0, 'L', 10);
     $pdf->setFillColor(230,230,230);
     $pdf->Cell(100, 15, utf8_decode('Responsável '), 1, 0, 'L', 10);
     $pdf->setFillColor(255,255,255);
     $pdf->Cell(130, 15, ltrim(utf8_decode($nomepessoa)), 1, 0, 'L', 10);
     $pdf->setFillColor(230,230,230);
     $pdf->Cell(50, 15, utf8_decode('E-mail '), 1, 0, 'L', 10);
     $pdf->setFillColor(255,255,255);
     $pdf->Cell(165, 15, ltrim(utf8_decode($email)), 1, 1, 'L', 10);
 }
$pdf->setFillColor(230,230,230);

//Linha referente a Contato
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(70, 15, utf8_decode('Telefone:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(120, 15, utf8_decode($telefone), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(80, 15, utf8_decode('Celular: '), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(110, 15, ltrim(utf8_decode($celular)), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
if($dadostipopessoa == 1){
    $pdf->Cell(70, 15, utf8_decode('Dt. Abertura:'), 1, 0, 'L', 10);
    $pdf->setFillColor(255,255,255);
    $pdf->Cell(115, 15, ltrim(utf8_decode($dataabertura)), 1, 1, 'L', 10);
}else if($dadostipopessoa == 0){
    $pdf->Cell(50, 15, utf8_decode('Dt. Nasc.:'), 1, 0, 'L', 10);
    $pdf->setFillColor(255,255,255);
    $pdf->Cell(135, 15, ltrim(utf8_decode($datanascimento)), 1, 1, 'L', 10);
}


//Cabeçalho a Instalação
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(565, 14, utf8_decode('ENDEREÇO DE INSTALAÇÃO:'), 1, 1, 'L', 10);

//Linha referente a Endereço e Bairro da Instalação
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(120, 15, utf8_decode('Endereço:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(200, 15, utf8_decode($rua . " - " . $numero), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(60, 15, utf8_decode('Bairro: '), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(185, 15, ltrim(utf8_decode($bairro)), 1, 1, 'L', 10);
//Linha referente a Endereço e Bairro da Instalação
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(80, 15, utf8_decode('Complemento:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(145, 15, utf8_decode($complemento), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(50, 15, utf8_decode('Cidade: '), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(130, 15, ltrim(utf8_decode($nomecidade)), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(30, 15, utf8_decode('UF: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(35, 15, ltrim(utf8_decode($uf)), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(30, 15, utf8_decode('CEP: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(65, 15, ltrim(utf8_decode($cep)), 1, 1, 'L', 10);

//Cabeçalho a Endereço de Cobrança
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(565, 14, utf8_decode('ENDEREÇO DE COBRANÇA:'), 1, 1, 'L', 10);

//Linha referente a Endereço e Bairro da Instalação
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(120, 15, utf8_decode('Endereço:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(200, 15, utf8_decode($rua . " - " . $numero), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(60, 15, utf8_decode('Bairro: '), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(185, 15, ltrim(utf8_decode($bairro)), 1, 1, 'L', 10);
//Linha referente a Endereço e Bairro da Instalação
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(80, 15, utf8_decode('Complemento:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(145, 15, utf8_decode($complemento), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(50, 15, utf8_decode('Cidade: '), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(130, 15, ltrim(utf8_decode($nomecidade)), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(30, 15, utf8_decode('UF: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(35, 15, ltrim(utf8_decode($uf)), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(30, 15, utf8_decode('CEP: '), 1, 0, 'C', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(65, 15, ltrim(utf8_decode($cep)), 1, 0, 'L', 10);

//Cabeçalho referente a OBJETO
$pdf->SetFont('Arial', 'B', 10);
$pdf->Ln(20);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(565, 14, utf8_decode('OBJETO:'), 1, 1, 'L', 10);

//Cabeçalho referente aos produtos
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(255,255,255);
$pdf->MultiCell(565,15, utf8_decode('Constitui-se objeto do presente instrumento a prestação, pela CONTRATADA em favor do CLIENTE, dos Serviços de
Conexão à internet (Serviços de Valor Adicionado), a serem disponibilizados nas dependências do CLIENTE, de acordo com
os termos e condições previstas no presente instrumento e no "Contrato de prestacao de serviços de conexão
À INTERNET E SERVIÇOS DE COMUNICAÇÃO MULTIMÍDIA", registrado ao Cartório da Comarca de Passo Fundo, 
sob o n.º 11.338, parte integrante e essencial à celebração deste instrumento. Para a disponibilização dos
Serviços de Conexão à internet (Serviços de Valor Adicionado) nas dependências do CLIENTE, a CONTRATADA obriga-se,
ainda, à prestação dos Serviços de Comunicação Multimídia (SCM), também objeto deste Contrato, de acordo com os termos
e condições previstas no presente instrumento e no "CONTRATO DE PRESTAÇÃO DE SERVIÇOS DE CONEXÃO À
INTERNET E SERVIÇOS DE COMUNICAÇÃO MULTIMÍDIA", registrado junto ao Cartório da Comarca de Passo Fundo -RS sob o n.º 11.338, parte integrante e essencial à celebração deste instrumento. '),1,1);

//Cabeçalho referente a OBJETO
$pdf->SetFont('Arial', 'B', 10);
$pdf->Ln(20);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(565, 14, utf8_decode('DO PLANO DE SERVIÇO CONTRATADO PELO CLIENTE'), 1, 1, 'C', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->Cell(565, 14, utf8_decode('INFORMAÇÕES DO PLANO CONTRATADO PELO CLIENTE'), 1, 1, 'C', 10);
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(80);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-94, 15, '', 0, 0, 'C', 0);
$pdf->Cell(100, 15, utf8_decode('Plano Contratado'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(250, 15, utf8_decode($tipoplano . " - ". $velocidadeplano), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->Cell(129, 15, utf8_decode('Valor Plano:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(100, 15, utf8_decode($valorplano), 1, 1, 'L', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(120, 15, utf8_decode('Prazo de Instalação:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(445, 15, utf8_decode('Dois dias úteis.'), 1, 1, 'L', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(100, 15, utf8_decode('Destinação do Plano:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);

//Verificar se a Destinação do Plano é para pessoa jurídica ou física
if($dadostipopessoa == 1){
    $textoassinaladojuridica = "(X)  ";
    $textoassinaladofisica = "( )  ";
}else if($dadostipopessoa == 0){
    $textoassinaladofisica = "(X)  ";
    $textoassinaladojuridica = "( )  ";
}
$pdf->Cell(225, 15, utf8_decode('       ' . $textoassinaladofisica . 'Residencial       |       ' . $textoassinaladojuridica . ' Empresarial'), 1, 0, 'L', 10);
$pdf->setFillColor(230,230,230);
$pdf->Cell(100, 15, utf8_decode('Vigência contratual:'), 1, 0, 'L', 10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(140, 15, utf8_decode('12 Meses'), 1, 0, 'L', 10);

$pdf->Ln(20);
$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-8, 15, '', 0, 0, 'C', 0);
$pdf->MultiCell(558,15, utf8_decode('As partes informam que foi firmado separadamente entre elas o CONTRATO DE PERMANÊNCIA, que vincula o
CLIENTE por prazo determinado, em troca da concessão de benefícios na contratação (ou renovação) dos serviços,
sob pena de pagamento pelo CLIENTE da multa prevista no próprio CONTRATO DE PERMANÊNCIA.'),1,1);

//Cabeçalho referente a Assinaturas
$pdf->SetFont('Arial', 'B', 10);
$pdf->Ln(20);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->setFillColor(230,230,230);
$pdf->Cell(565, 14, utf8_decode('CONDIÇÕES DE DEGRADAÇÃO OU INTERRUPÇÃO DOS SERVIÇOS PRESTADOS: '), 1, 1, 'C', 10);
$pdf->Ln(10);

//Contorno Primeira Assinatura
$pdf->Line(300, 800, 15, 800);
$pdf->Line(300, 800, 300, 720);
$pdf->Line(14, 800, 15, 720);
$pdf->Line(300, 720, 15, 720);

//Contorno Segunda Assinatura
$pdf->Line(300, 800, 578, 800);
$pdf->Line(300, 720, 578, 720);
$pdf->Line(578, 800, 578, 720);

$pdf->SetFont('Arial', 'B', 10);
$pdf->Cell(-14, 15, '', 0, 0, 'C', 0);
$pdf->Ln(10);

date_default_timezone_set('America/Sao_Paulo');
$datacontrato = date('d/m/Y');

if($dadostipopessoa == 0){
    $pdf->Ln();
}
$pdf->Cell(35, 14, utf8_decode('DATA:'), 0, 0, 'L');
$pdf->Cell(70, 14, $datacontrato, 0, 0, 'L');
$pdf->Cell(55, 14, utf8_decode('CPF/CNPJ:'), 0, 0, 'L');
$pdf->Cell(115, 14, utf8_decode('60.607.649/0001-05'), 0, 0, 'L');
$pdf->Line(30, 775, 275, 775);

$pdf->Cell(33, 8, utf8_decode('DATA:'), 0, 0, 'L');
$pdf->Cell(70, 8, $datacontrato, 0, 0, 'L');
$pdf->Cell(55, 8, utf8_decode('CPF/CNPJ:'), 0, 0, 'L');
$pdf->Cell(8, 8, $cpfcnpj, 0, 0, 'L');

if($dadostipopessoa == 1){
    $pdf->Ln(57);
}else if($dadostipopessoa == 0){
    $pdf->Ln(56);
}

$pdf->Cell(240, 2, utf8_decode('Assinatura (CONTRATADA)'), 0, 0, 'C');
$pdf->Cell(345, 2, utf8_decode('Assinatura (CLIENTE)'), 0, 0, 'C');
$pdf->Line(320, 775, 548, 775);

$pdf->Output("contrato-". $cpfcnpj .".pdf", "D");

pg_close($conexao);