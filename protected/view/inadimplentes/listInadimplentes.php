<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Relação de Inadimplentes</h2>
                <div class="clearfix"></div>
            </div>
            <br/><br/>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 250px;">Nome</th>
                            <th style="text-align: center; width: 150px">Contato</th>
                            <th style="width: 250px;">E-mail</th>
                            <th>Plano Contratado</th>
                            <th style="text-align: center; width: 100px">Data Vencimento</th>
                            <th style="text-align: center">Valor</th>
                        </tr>
                    </thead>

                    <tbody> 
                        <?php
                            foreach ($listaDados as $item) {
                                echo '<td>' . $item['nomepessoa'];
                                echo '<td style="text-align: center">' . $item['contatopessoa'];
                                echo '<td>' . $item['emailpessoa'];
                                echo '<td>' . $item['planocontratado'];
                                echo '<td style="text-align: center">' . $item['datavencimento'];
                                echo '<td style="text-align: center">' . $item['valorboleto'];
                                echo '</tr>';
                            }
                        ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>