<?php
    require_once("../../../config/confloginrel.php");
    session_start();
    /*if(isset($_SESSION['cpf']) != ''){
        $sessao_cpf = $_SESSION['cpf'];
    }else{
        echo "<script>top.location.href='../../../acesso/login.php';</script>";
    }*/
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Softnet</title>
        <link href="../../../vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        
        <style>
            #menutitle{
                color: white;
                padding-left: 25px;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #5A738E">
            <div class="container" style="padding-left: 0px">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#" style="height: 30px; width: 300px;"  id="menutitle">Filtro Relatório por Área</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse" style="width: 120%;"></div>
            </div>
        </nav>
        <br/><br/><br/>
        <div class="container">
            <form name="filtrorelatoriopessoaplano" id="filtrorelatoriopessoaplano" action="rel_pessoa_plano.php" method="post">
            <div class="row">
                <div class="col-xs-8">
                    <label for="tipopesosa">Tipo Pessoa </label><br/>
                    <center>
                    <b>
                    <input type="radio" name="tipopessoa" value="PF"> Pessoa Física
                    <input type="radio" name="tipopessoa" value="PJ"> Pessoa Jurídica
                    </b>
                    </center>
                </div>
            </div>            
            <br/>
            <button type="submit" class="btn btn-success">Gerar Relatório</button>
            <button type="reset" class="btn btn-primary">Limpar</button>
        </form>
    </div>
    </body>
</html>