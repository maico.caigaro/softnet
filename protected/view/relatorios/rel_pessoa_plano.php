<?php
include '../../../config/confloginrel.php';
require_once("../../fpdf/fpdf.php");
//P = Paisagem
//L = Retrato
$pdf = new FPDF("L", "pt", "A4");

$pdf->AddPage('L');
$pdf->Ln(3);

$tipopessoa = $_POST['tipopessoa'];

if($tipopessoa == 'PF'){
    $colunassql = 'pe.nome as nomepessoa,
                   pf.rg,
                   pf.cpf,';
    $filtro = 'inner join pessoafisica pf
                  on pf.idpessoa = pe.id
               where pf.cpf is not null';
}else{
    $colunassql = 'pj.razaosocial as razaosocial,
                   pj.cnpj,
                   pj.inscricaoestadual,';
    $filtro = 'inner join pessoajuridica pj
                  on pj.idpessoa = pe.id 
               where pj.cnpj is not null';
}

//Fazer o sql para buscar as informações do boleto
$querypessoaboleto = "select upper(ser.tipoplano || ' - ' || ser.velocidadeplano) as tipoplano,
                            'R$ ' || LTRIM(to_char(ser.valorplano, '9G999G990D99')) as valorplano,
                            $colunassql
                            ende.bairro,
                            ende.rua,
                            ende.numero,
                            ende.cep,
                            ende.ativo,
                            pe.telefone,
                            pe.celular,
                            pe.email,
                            (cid.nome || ' - ' || est.uf) as nomecidadeuf
                       from agendamento agend
                      inner join endereco ende
                         on agend.idendereco = ende.id 
                      inner join servico ser
                         on agend.idservico = ser.id
                      inner join tiposervicos tipo
                         on agend.idtiposervico =  tipo.id
                      inner join pessoa pe 
                         on pe.idendereco = ende.id
                      inner join cidade cid
                         on ende.idcidade = cid.id
                      inner join estado est
                         on cid.idestado = est.id
                            $filtro";
$resultpessoaplano = pg_query($querypessoaboleto);

//Imagem do logo Softnet
$pdf->Image('../../imagens/logo_softnet.png', +20,30,110);

if($tipopessoa == 'PF'){
    $pdf->SetFont('arial', 'B', 14);
    $pdf->Cell(120);
    $pdf->Cell(550, 7, utf8_decode('Relatório de Plano Pessoa Física'), 0, 0, 'C');
    $pdf->SetFont('arial', 'B', 11);
    $pdf->Cell(-70, 7, utf8_decode(''), 0, 0, 'R');
    $pdf->SetFont('arial', '', 10);
    $pdf->Ln();
}else{
    $pdf->SetFont('arial', 'B', 14);
    $pdf->Cell(120);
    $pdf->Cell(550, 7, utf8_decode('Relatório de Plano Pessoa Jurídica'), 0, 0, 'C');
    $pdf->SetFont('arial', 'B', 11);
    $pdf->Cell(-70, 7, utf8_decode(''), 0, 0, 'R');
    $pdf->SetFont('arial', '', 10);
    $pdf->Ln();
}
    
//CABÇALHO RELATÓRIO CULTURA
$pdf->SetFont('Arial', 'B', 12);
$pdf->Ln(60);
$pdf->SetFont('Arial', 'B', 8);
$pdf->setFillColor(180, 180, 180);

if($tipopessoa == 'PF'){
    $pdf->Cell(-10);
    $pdf->Cell(130, 14, utf8_decode('Nome Pessoa'), 1, 0, 'L', 1);
    $pdf->Cell(70, 14, utf8_decode('Contato'), 1, 0, 'L', 1);
    $pdf->Cell(135, 14, utf8_decode('E-mail'), 1, 0, 'L', 1);
    $pdf->Cell(50, 14, utf8_decode('Situação'), 1, 0, 'C', 1);
    $pdf->Cell(100, 14, utf8_decode('Cidade/UF'), 1, 0, 'L', 1);
    $pdf->Cell(90, 14, utf8_decode('Bairro'), 1, 0, 'L', 1);
    $pdf->Cell(120, 14, utf8_decode('Endereço'), 1, 0, 'L', 1);
    $pdf->Cell(100, 14, utf8_decode('Tipo Plano'), 1, 0, 'C', 1);
    $pdf->SetFont('Arial', '', 8);
    $pdf->Ln();
    while ($consulta = pg_fetch_assoc($resultpessoaplano)) {
        $pdf->setFillColor(255,255,255);
        if($consulta["telefone"] == ''){
            $contato = $consulta["celular"];
        }else{
            $contato = $consulta["telefone"];
        }

        if($consulta["ativo"] == 'S'){
            $situacaoativo = 'Ativo';
        }else{
            $situacaoativo = 'Não Ativo';
        }
        if($consulta["email"] != ''){
            $contememail = $consulta["email"];
        }else{
            $contememail = 'E-mail não informado';
        }
            
        $pdf->Cell(-10);
        $pdf->Cell(130, 14, utf8_decode($consulta["nomepessoa"]), 1, 0, 'L', 1);
        $pdf->Cell(70, 14, utf8_decode($contato), 1, 0, 'L', 1);
        $pdf->Cell(135, 14, utf8_decode($consulta["email"]), 1, 0, 'L', 1);
        $pdf->Cell(50, 14, utf8_decode($situacaoativo), 1, 0, 'C', 1);
        $pdf->Cell(100, 14, utf8_decode($consulta["nomecidadeuf"]), 1, 0, 'L', 1);
        $pdf->Cell(90, 14, utf8_decode($consulta["bairro"]), 1, 0, 'L', 1);
        $pdf->Cell(120, 14, utf8_decode($consulta["rua"]), 1, 0, 'L', 1);
        $pdf->Cell(100, 14, utf8_decode($consulta["tipoplano"]), 1, 0, 'C', 1);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Ln();
    }
}else{
    $pdf->Cell(-10);
    $pdf->Cell(130, 14, utf8_decode('Razão Social'), 1, 0, 'L', 1);
    $pdf->Cell(70, 14, utf8_decode('Contato'), 1, 0, 'L', 1);
    $pdf->Cell(135, 14, utf8_decode('E-mail'), 1, 0, 'L', 1);
    $pdf->Cell(50, 14, utf8_decode('Situação'), 1, 0, 'C', 1);
    $pdf->Cell(100, 14, utf8_decode('Cidade/UF'), 1, 0, 'L', 1);
    $pdf->Cell(90, 14, utf8_decode('Bairro'), 1, 0, 'L', 1);
    $pdf->Cell(120, 14, utf8_decode('Endereço'), 1, 0, 'L', 1);
    $pdf->Cell(100, 14, utf8_decode('Tipo Plano'), 1, 0, 'C', 1);
    $pdf->SetFont('Arial', '', 8);
    $pdf->Ln();
    while ($consulta = pg_fetch_assoc($resultpessoaplano)) {
        $pdf->setFillColor(255,255,255);
        if($consulta["telefone"] == ''){
            $contato = $consulta["celular"];
        }else{
            $contato = $consulta["telefone"];
        }

        if($consulta["ativo"] == 'S'){
            $situacaoativo = 'Ativo';
        }else{
            $situacaoativo = 'Não Ativo';
        }
        if($consulta["email"] != ''){
            $contememail = $consulta["email"];
        }else{
            $contememail = 'E-mail não informado';
        }
            
        $pdf->Cell(-10);
        $pdf->Cell(130, 14, utf8_decode($consulta["razaosocial"]), 1, 0, 'L', 1);
        $pdf->Cell(70, 14, utf8_decode($contato), 1, 0, 'L', 1);
        $pdf->Cell(135, 14, utf8_decode($consulta["email"]), 1, 0, 'L', 1);
        $pdf->Cell(50, 14, utf8_decode($situacaoativo), 1, 0, 'C', 1);
        $pdf->Cell(100, 14, utf8_decode($consulta["nomecidadeuf"]), 1, 0, 'L', 1);
        $pdf->Cell(90, 14, utf8_decode($consulta["bairro"]), 1, 0, 'L', 1);
        $pdf->Cell(120, 14, utf8_decode($consulta["rua"]), 1, 0, 'L', 1);
        $pdf->Cell(100, 14, utf8_decode($consulta["tipoplano"]), 1, 0, 'C', 1);
        $pdf->SetFont('Arial', '', 8);
        $pdf->Ln();
    }
}

//Contorno Margens do relatório
$pdf->Line(830, 10, 10, 10);
$pdf->Line(10, 575, 10, 10);
$pdf->Line(830, 575, 830, 10);
$pdf->Line(830, 575, 10, 575);




$pdf->SetFont('Arial', 'U', 10);




$pdf->SetFont('Arial', 'B', 10);
$pdf->Ln(250);
$pdf->setFillColor(255,255,255);
$pdf->Cell(240);
$pdf->Cell(500, 15, utf8_decode('Data Geração Relatório: '), 0, 0, 'R', 10);
$datageracaoboleto = date('d/m/Y');
$pdf->Cell(50, 15, utf8_decode($datageracaoboleto), 0, 0, 'L', 10);

$pdf->Output("relatorio_pessoa_plano". $consulta["nomepessoa"] .".pdf", "D");

pg_close($conexao);