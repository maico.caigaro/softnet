<?php
include '../../../config/confloginrel.php';
require_once("../../fpdf/fpdf.php");

//P = Paisagem
//L = Retrato
$pdf = new FPDF("L", "pt", "A4");

$pdf->AddPage('P');
$pdf->Ln(3);

//Esse valor vai vir através do GET
$idboleto =base64_decode($_GET['id']);

//Fazer o sql para buscar as informações do boleto
$queryboleto = "select bo.id,
                        to_char(bo.datavencimento, 'dd/MM/yyyy') as datavencimento,
                        'R$ ' || LTRIM(to_char(bo.valor, '9G999G990D99')) as valorboleto,
                        to_char(bo.datapagamento, 'dd/MM/yyyy') as datapagamento,
                        upper(ser.tipoplano || ' - ' || ser.velocidadeplano) as tipoplano,
                        pe.nome as nomepessoa,
                        pj.razaosocial as razaosocial
                   from boleto bo
                  inner join pessoa pe
                     on bo.idpessoa = pe.id 
                  inner join contrato con
                     on bo.idcontrato = con.id
                  inner join agendamento agen
                     on con.idagendamento = agen.id
                  inner join servico ser
                     on agen.idservico = ser.id
                   left join pessoafisica pf
                     on pe.id = pf.idpessoa
                   left join pessoajuridica pj
                     on pe.id = pj.idpessoa
                  where bo.id = $idboleto";
$resultboleto = pg_query($queryboleto);
    
while ($consulta = pg_fetch_assoc($resultboleto)) {
   $datavencimento = $consulta["datavencimento"];
   $valorboleto    = $consulta["valorboleto"];
   $datapagamento  = $consulta["datapagamento"];
   $tipoplano      = $consulta["tipoplano"];
   $nomepessoa     = $consulta["nomepessoa"];
   $razaosocial    = $consulta["razaosocial"];
   if($nomepessoa == ''){
       $nomecliente = $razaosocial;
   }else{
       $nomecliente = $nomepessoa;
   }
}

//Gerar caracteres aleatórios para o Número do documento
$str = "ABCD1234567890";
$numerodocumento = str_shuffle($str);

//Contorno Margens do relatório
$pdf->Line(585, 10, 10, 10);
$pdf->Line(10, 825, 10, 10);
$pdf->Line(585, 825, 585, 10);
$pdf->Line(585, 825, 10, 825);

//Imagem do logo Softnet
$pdf->Image('../../imagens/logo_softnet.png', +20,30,110);


$pdf->SetFont('Arial', 'U', 10);

//Primeiro contorno
$pdf->Line(300, 100, 15, 100);
$pdf->Line(300, 170, 15, 170);
$pdf->Line(14, 100, 15, 170);
$pdf->Line(300, 100, 300, 170);

//Segundo contorno
$pdf->Line(480, 100, 170, 100);
$pdf->Line(250, 170, 480, 170);
$pdf->Line(480, 100, 480, 170);

//Terceiro contorno
$pdf->Line(580, 100, 170, 100);
$pdf->Line(580, 170, 430, 170);
$pdf->Line(580, 100, 580, 170);

//Informações para colocar nos quadrantes do boleto
$pdf->SetFont('Arial', '', 12);
$pdf->Ln(70);
$pdf->Cell(-10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(120, 15, utf8_decode('Cedente'), 0, 0, 'L', 10);
$pdf->Ln(20);
$pdf->Cell(-10);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(120, 15, utf8_decode('Softnet Ltda - CNPJ:'), 0, 0, 'L', 10);
$pdf->Cell(166);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(80, 15, utf8_decode('Agência/Código Cedente'), 0, 0, 'L', 10);
$pdf->Cell(100);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(80, 15, utf8_decode('Vencimento'), 0, 0, 'L', 10);
$pdf->Ln(20);
$pdf->Cell(-10);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(120, 15, utf8_decode('60.607.649/0001-05'), 0, 0, 'L', 10);
$pdf->Cell(166);
$pdf->Cell(80, 15, utf8_decode('0230.00.100783'), 0, 0, 'L', 10);
$pdf->Cell(100);
$pdf->Cell(80, 15, utf8_decode($datavencimento), 0, 0, 'L', 10);
$pdf->SetFont('Arial', '', 12);

//Primeiro contorno
$pdf->Line(300, 230, 15, 230);
$pdf->Line(15, 230, 15, 170);
$pdf->Line(300, 230, 300, 170);

//Segundo contorno
$pdf->Line(480, 230, 170, 230);
$pdf->Line(480, 230, 480, 170);

//Terceiro contorno
$pdf->Line(580, 230, 170, 230);
$pdf->Line(580, 230, 580, 170);

//Informações para colocar nos quadrantes do boleto
$pdf->SetFont('Arial', '', 12);
$pdf->Ln(30);
$pdf->Cell(-10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(120, 15, utf8_decode('Cliente'), 0, 0, 'L', 10);
$pdf->Ln(20);
$pdf->Cell(-10);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(120, 5, utf8_decode(''), 0, 0, 'L', 10);
$pdf->Cell(166);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(80, 15, utf8_decode('Número do Documento'), 0, 0, 'L', 10);
$pdf->Cell(100);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(80, 15, utf8_decode('Nosso Número'), 0, 0, 'L', 10);
$pdf->Ln(20);
$pdf->Cell(-10);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(120, 15, utf8_decode($nomecliente), 0, 0, 'L', 2);
$pdf->Cell(166);
$pdf->Cell(80, 15, utf8_decode($numerodocumento), 0, 0, 'L', 10);
$pdf->Cell(100);
$pdf->Cell(80, 15, utf8_decode('15/302657-0'), 0, 0, 'L', 10);
$pdf->SetFont('Arial', '', 12);

//Primeiro contorno
$pdf->Line(120, 300, 15, 300);
$pdf->Line(15, 230, 15, 300);
$pdf->Line(120, 230, 120, 300);

//Segundo contorno
$pdf->Line(200, 300, 15, 300);
$pdf->Line(200, 230, 200, 300);

//Terceiro contorno
$pdf->Line(300, 300, 15, 300);
$pdf->Line(300, 230, 300, 300);

//Quarto contorno
$pdf->Line(480, 300, 15, 300);
$pdf->Line(480, 230, 480, 300);

//Quinta contorno
$pdf->Line(580, 300, 15, 300);
$pdf->Line(580, 230, 580, 300);

//Informações para colocar nos quadrantes do boleto
$pdf->SetFont('Arial', '', 12);
$pdf->Ln(30);
$pdf->Cell(-10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(80, 15, utf8_decode('Data Pagamento'), 0, 0, 'L', 10);
$pdf->Cell(25);
$pdf->Cell(75, 15, utf8_decode('Quantidade'), 0, 0, 'L', 10);
$pdf->Cell(18);
$pdf->Cell(80, 15, utf8_decode('(x) Valor'), 0, 0, 'L', 10);
$pdf->Cell(25);
$pdf->Cell(80, 15, utf8_decode('Situação'), 0, 0, 'L', 10);
$pdf->Cell(100);
$pdf->Cell(80, 15, utf8_decode('(-) Desconto'), 0, 1, 'L', 10);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Ln(10);
$pdf->Cell(-10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(80, 15, utf8_decode($datapagamento), 0, 0, 'L', 10);
$pdf->Cell(25);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(75, 15, utf8_decode('1'), 0, 0, 'C', 10);
$pdf->Cell(18);
$pdf->Cell(80, 15, utf8_decode($valorboleto), 0, 0, 'L', 10);
$pdf->Cell(25);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(80, 15, utf8_decode('Pago'), 0, 0, 'L', 10);
$pdf->Cell(100);
$pdf->Cell(80, 15, utf8_decode('R$ 0,00'), 0, 1, 'L', 10);

//Primeiro contorno
$pdf->Line(300, 350, 15, 350);
$pdf->Line(15, 230, 15, 350);
$pdf->Line(300, 230, 300, 350);

//Segundo contorno
$pdf->Line(480, 350, 15, 350);
$pdf->Line(480, 230, 480, 350);

//Terceiro contorno
$pdf->Line(580, 350, 15, 350);
$pdf->Line(580, 230, 580, 350);

//Informações para colocar nos quadrantes do boleto
$pdf->SetFont('Arial', '', 12);
$pdf->Ln(25);
$pdf->Cell(-10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(80, 15, utf8_decode(''), 0, 0, 'L', 10);
$pdf->Cell(4);
$pdf->Cell(80, 15, utf8_decode(''), 0, 0, 'L', 10);
$pdf->Cell(18);
$pdf->Cell(80, 15, utf8_decode(''), 0, 0, 'L', 10);
$pdf->Cell(25);
$pdf->Cell(80, 15, utf8_decode('(+) Outros Acréscimos'), 0, 0, 'L', 10);
$pdf->Cell(95);
$pdf->Cell(70, 15, utf8_decode('(=) Valor Cobrado'), 0, 1, 'L', 10);

$pdf->SetFont('Arial', 'B', 12);
$pdf->Ln(10);
$pdf->Cell(-10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(90, 15, utf8_decode('Demonstrativo:'), 0, 0, 'L', 10);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(80, 15, utf8_decode(''), 0, 0, 'L', 10);
$pdf->Cell(18);
$pdf->Cell(80, 15, utf8_decode(''), 0, 0, 'L', 10);
$pdf->Cell(25);
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(80, 15, utf8_decode('Não há outros acrescimos'), 0, 0, 'L', 10);
$pdf->Cell(100);
$pdf->Cell(80, 15, utf8_decode($valorboleto), 0, 1, 'L', 10);

//Primeiro contorno
$pdf->Line(580, 500, 15, 500);
$pdf->Line(15, 230, 15, 500);
$pdf->Line(580, 230, 580, 500);

$pdf->SetFont('Arial', '', 12);
$pdf->Ln(10);
$pdf->Cell(-10);
$pdf->setFillColor(255,255,255);
$pdf->Cell(90, 15, utf8_decode('SERVIÇOS DE INTERNET'), 0, 1, 'L', 10);
$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(-10);
$pdf->Ln(10);
$pdf->Cell(350, 15, utf8_decode($tipoplano), 0, 0, 'L', 10);
$pdf->Cell(60);
$pdf->Cell(40, 15, utf8_decode($valorboleto), 0, 0, 'L', 10);

$pdf->SetFont('Arial', 'B', 10);
$pdf->Ln(250);
$pdf->setFillColor(255,255,255);
$pdf->Cell(500, 15, utf8_decode('Data Geração Boleto: '), 0, 0, 'R', 10);
$datageracaoboleto = date('d/m/Y');
$pdf->Cell(50, 15, utf8_decode($datageracaoboleto), 0, 0, 'L', 10);

$pdf->Output("boleto". $nomecliente . $idboleto .".pdf", "D");

pg_close($conexao);