<?php
    if(isset($_POST['idpessoa'])){
        $idpessoa = $_POST['idpessoa'];
    }else{
        $idpessoa = "";
    }
?>
<?php if (isset($_GET['updt']) == 'dXBkYXRlc3RhdHVz') { ?>
    <div class="alert alert-success">
        Dados atualizados com <strong>Sucesso</strong>
    </div>
<?php } else if (isset($_GET['updterror']) == 'dXBkYXRlZXJybw==') { ?>
    <div class="alert alert-danger">
        Não foi possível atualizar o registro.
    </div>
<?php } else if (isset($_GET['dlt']) == 'ZGVsZXRlc3VjZXNz') { ?>
    <div class="alert alert-success">
        Registro Removido com <strong>Sucesso</strong>
    </div>
<?php } else if (isset($_GET['inserterror']) == 'aW5zZXJ0ZXJyb3I=') { ?>
    <div class="alert alert-danger">
        Não foi possível inserir o registro.
    </div>
<?php } else if (isset($_GET['insertok']) == 'aW5zZXJ0c3VjZXNz') { ?>
    <div class="alert alert-success">
        Pagamento realizado com sucesso
    </div>
<?php }
?>
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Filtrar Boleto</h2>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <h5><b>Filtrar todos os boletos específicos do cliente somente para Leitura</b></h5>
                </div>
                <div class="x_content">
                    <br />
                    <form action="<?php echo $acao;?>" name="formBoleto" id="formBoleto" method="POST" role="form" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Cliente</label>
                            <div class="col-md-6 col-sm-9 col-xs-12">
                              <select class="select2_single form-control" name="idpessoa" id="idpessoa">
                                  <?php
                                    if($idpessoa==''){?>
                                        <option value="">Selecione o Cliente</option>
                                    <?php }
                                  ?>
                                <?php
                                    foreach ($listaPessoasBoleto as $pessoas) {
                                        $selected = (isset($boleto) && $boleto['idpessoa'] == $pessoas['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $pessoas['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                <?php echo $pessoas['nomepessoa']; ?>
                                        </option>
                                <?php } ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <input type="submit" class="btn btn-success" value="Filtrar" />
                                <input class="btn btn-info" type="reset" value="Limpar" />
                                <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Voltar</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Relação de Boletos</h2>
                    <div class="clearfix"></div>
                </div>

                <br/><br/>
                <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 200px;">Nome Pessoa</th>
                                <th style="width: 120px;">Telefone</th>
                                <th style="width: 80px; text-align: center;">Data Vencimento</th>
                                <th style="width: 80px; text-align: center;">Data Pagamento</th>
                                <th style="width: 80px; text-align: center;">Valor</th>
                                <th style="width: 200px;">Tipo Serviço</th>
                                <th style="text-align: center; width: 40px;">Situação Pagamento</th>
                                <th style="text-align: center; width: 40px;">Alterar</th>
                                <th style="text-align: center;">Gerar Boleto</th>
                            </tr>
                        </thead>

                        <tbody> 
                            <?php
                            
                            foreach ($listaDados as $item) {
                                echo '<td>' . $item['nomepessoa'];
                                echo '<td>' . $item['telefone'];
                                echo '<td>' . $item['datavencimento'];
                                echo '<td>' . $item['datapagamento'];
                                echo '<td>' . $item['valor'];
                                echo '<td>' . $item['descricaotiposervico'];
                                $datapagamento = $item['datapagamento'];
                                $id = base64_encode($item['id']);
                                $idpessoa = $item['idpessoa'];
                                
                                $dataatual = date('d/m/Y');
                                if($datapagamento == ''){
                                    $imagemcomprovante = "<img src='http://localhost/softnet/protected/imagens/comprovantedesabilitado.png' style='width: 28px;'/>";
                                if($_GET['acao'] == 'listar'){
                                        if($item['datavencimento'] < $dataatual){
                                            $situationpayment = 'Pendente';
                                            $btnsituationpayment = 'btn-warning';
                                        }else{
                                            $situationpayment = 'Pagar';
                                            $btnsituationpayment = 'btn-success';
                                        }
                                        //Não esta filtrando por pessoa especifica
                                        $situacaoboleto = "<a href='index.php?controle=boletoController&acao=filtrarboleto&id=$id' title='Clique aqui para pagar o boleto'"
                                                        . " class='btn $btnsituationpayment btn-xs'><i class='fa fa'></i> $situationpayment </a>";
                                    }else{
                                        //Se não estará filtrando por pessoa específica utilizando o select da pessoa
                                        $situacaoboleto = "<a title='Clique aqui para pagar o boleto'"
                                                    . " class='btn btn-warning btn-xs'><i class='fa fa'></i> Pendente </a>";
                                    }
                                }else{
                                    if($item['datapagamento'] != ''){
                                        $corbotaopagamento = 'btn-primary';
                                    }
                                    $imagemcomprovante = "
                                    <a href='protected/view/boleto/arq_boleto.php?id=$id' target='_blank'></a>
                                    <img src='http://localhost/softnet/protected/imagens/comprovante.png' style='width: 28px;'/>";
                                    $situacaoboleto = "<title='O boleto já está pago'"
                                                    . " class='btn $corbotaopagamento btn-xs'><i class='fa fa'></i> Pago";
                                }
                                echo "<td style='text-align: center;'> $situacaoboleto </td>";
                                echo "<td style='text-align: center;'> <a href='index.php?controle=boletoController&acao=buscar&id=$id' title='Clique aqui para alterar o registro'"
                                . " class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Alterar </a>"
                                . "</a> </td>";
                                echo '<td style="text-align: center">' . $imagemcomprovante ."</a>";
                                echo '</tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
<script src="src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="src/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $("#formBoleto").validate({
        rules: {
            idpessoa: {
                required: true
            }
        },
        messages: {
            idpessoa: {
                required: "Por favor, Selecione a Pessoa"
            }
        }
    });
</script>