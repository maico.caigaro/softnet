<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cadastro de Boleto</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form action="<?php echo $acao; ?>" name="formBoleto" id="formBoleto" method="POST" role="form" data-parsley-validate class="form-horizontal form-label-left">
                        <input type="hidden" name="id" value="<?php if (isset($boleto)) echo $boleto['id']; ?>"/>
                        <?php
                            if(isset($estado['id']) != ''){
                                $alterando = 1;
                            }else{
                                $alterando = 0;
                            }
                        ?>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Pessoa</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="select2_single form-control" name="idpessoa" id="idpessoa" required data-errormessage-value-missing="Selecione a Pessoa">
                                  <option value="">Selecione a Pessoa</option>
                                    <?php
                                    foreach ($listaPessoas as $pessoas) {
                                        $selected = (isset($boleto) && $boleto['idpessoa'] == $pessoas['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $pessoas['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $pessoas['nomepessoa']; ?>
                                        </option>
                                    <?php } ?>
                              </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Tipo de Serviço</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="select2_single form-control" name="idtiposervico" id="idtiposervico" required data-errormessage-value-missing="Selecione o Tipo de Serviço">
                                  <option value="">Selecione o Tipo de Serviço</option>
                                    <?php
                                    foreach ($listaServicos as $servicos) {
                                        $selected = (isset($boleto) && $boleto['idservico'] == $servicos['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $servicos['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $servicos['tipoplano'] . ' - ' . $servicos['velocidadeplano']; ?>
                                        </option>
                                    <?php } ?>
                              </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Contrato</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="select2_single form-control" name="idcontrato" id="idcontrato" required data-errormessage-value-missing="Selecione o Contrato">
                                  <option value="">Selecione o Contrato</option>
                                    <?php
                                    foreach ($listaContratos as $contratos) {
                                        $selected = (isset($boleto) && $boleto['idcontrato'] == $contratos['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $contratos['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $contratos['descricaoservico']; ?>
                                        </option>
                                    <?php } ?>
                              </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datavencimento">Data Vencimento<span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-8">
                                <input type="text" id="datavencimento" name="datavencimento" placeholder="Informe a Data de Vencimento"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($boleto)) echo $boleto['datavencimento']; ?>" disabled="" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group" id="valor">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="valor">Valor <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-8">
                                <input type="text" id="valor" name="valor" placeholder="Valor"
                                       autocomplete="off" value="<?php if (isset($boleto)) echo $boleto['valor']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                              <input type="submit" class="btn btn-success" value="Gravar" />
                              <?php
                                if($alterando == 1){?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-primary disabled" type="reset" value="Limpar" />
                                <?php } else {?>
                                    <input class="btn btn-info" type="reset" value="Limpar" />
                                <?php }
                              ?>
                          </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="src/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $("#formBoleto").validate({
        rules: {
            idpessoa: {
                required: true
            }
        },
        messages: {
            idpessoa: {
                required: "Por favor, Selecione a Pessoa"
            }
        }
    });
</script>