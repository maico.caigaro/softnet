<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cadastro de Cidade</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form action="<?php echo $acao; ?>" name="formCidade" id="formCidade" method="POST" role="form" data-parsley-validate class="form-horizontal form-label-left">
                        <?php
                            if(isset($cidade['id']) != ''){
                                $alterando = 1;
                            }else{
                                $alterando = 0;
                            }
                        ?>
                        <input type="hidden" name="id" value="<?php if (isset($cidade)) echo $cidade['id']; ?>"/>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nome">Nome <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nome" name="nome" placeholder="Informe o Nome"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($cidade)) echo $cidade['nomecidade']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Estado</label>
                        <div class="col-md-2 col-sm-2 col-xs-6">
                          <select class="select2_single form-control" name="idestado" id="idestado" required data-errormessage-value-missing="Selecione a UF">
                              <option value="">Selecione a UF</option>
                            <?php
                            foreach ($listaEstados as $estados) {
                                $selected = (isset($cidade) && $cidade['idestado'] == $estados['id']) ? 'selected' : '';
                                ?>
                                <option value='<?php echo $estados['id']; ?>'
                                        <?php echo $selected; ?>> 
                                            <?php echo $estados['uf']; ?>
                                </option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                              <input type="submit" class="btn btn-success" value="Gravar" />
                              <?php
                                if($alterando == 1){?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-primary disabled" type="reset" value="Limpar" />
                                <?php } else {?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-info" type="reset" value="Limpar" />
                                <?php }
                              ?>
                          </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="src/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $("#formCidade").validate({
        rules: {
            nome: {
                required: true,
                minlength: 3,
                maxlength: 80
            },
            idestado: {
                required: true
            }
        },
        messages: {
            nome: {
                required: "Por favor, informe o Nome da Cidade",
                minlength: "O Nome deve ter pelo menos 3 caracteres",
                maxlength: "O Nome deve ter no máximo 80 caracteres"
            },
            idestado: {
                required: "Por favor, selecione a UF"
            }
        }
    });
</script>