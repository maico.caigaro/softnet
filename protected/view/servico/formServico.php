<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cadastro Serviço</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form action="<?php echo $acao; ?>" name="formServico" id="formServico" method="POST" role="form" data-parsley-validate class="form-horizontal form-label-left">
                        <?php
                            if(isset($servico['id']) != ''){
                                $alterando = 1;
                            }else{
                                $alterando = 0;
                            }
                        ?>
                        <input type="hidden" name="id" value="<?php if (isset($servico)) echo $servico['id']; ?>"/>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="tipoplano">Tipo Plano <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="tipoplano" name="tipoplano" placeholder="Informe o Tipo do Plano"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($servico)) echo $servico['tipoplano']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="velocidadeplano">Velocidade Plano <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="velocidadeplano" name="velocidadeplano" placeholder="Informe a Velocidade do Plano"
                                       autocomplete="off" maxlength="30" value="<?php if (isset($servico)) echo $servico['velocidadeplano']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="tipoplano">Valor Plano <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-8">
                                <input type="text" id="valorplano" name="valorplano" placeholder="Valor do Plano"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($servico)) echo $servico['valorplano']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                              <input type="submit" class="btn btn-success" value="Gravar" />
                              <?php
                                if($alterando == 1){?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-primary disabled" type="reset" value="Limpar" />
                                <?php } else {?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-info" type="reset" value="Limpar" />
                                <?php }
                              ?>
                          </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="src/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $("#formServico").validate({
        rules: {
            tipoplano: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            velocidadeplano: {
                required: true,
                minlength: 3,
                maxlength: 30
            },
            valorplano: {
                required: true
            }
        },
        messages: {
            tipoplano: {
                required: "Por favor, informe o Tipo do Plano do Serviço",
                minlength: "O Tipo de Plano deve ter pelo menos 3 caracteres",
                maxlength: "O Tipo de Plano deve ter no máximo 100 caracteres"
            },
            velocidadeplano: {
                required: "Por favor, informe a Velocidade do Plano",
                minlength: "A Velocidade do Plano deve ter pelo menos 3 caracteres",
                maxlength: "A Velocidade do Plano deve ter no máximo 30 caracteres"
            },
            valorplano: {
                required: "Por favor, informe o Valor do Plano"
            }
        }
    });
</script>