<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cadastro de Pessoa Fisica</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form action="<?php echo $acao; ?>" name="formPessoafisica" id="formPessoafisica" method="POST" role="form" data-parsley-validate class="form-horizontal form-label-left">
                        <?php
                            //Verificar se estou recebendo o ID, se estiver recebendo quer dizer que estou editando
                            if(isset($pessoafisica['id']) != ''){
                                $alterando = 1;
                            }else{
                                $alterando = 0;
                            }
                        ?>
                        <input type="hidden" name="id" value="<?php if (isset($pessoafisica)) echo $pessoafisica['id']; ?>"/>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nome">Nome <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="nome" name="nome" placeholder="Informe o Nome"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoafisica)) echo $pessoafisica['nome']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">E-mail <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="email" id="email" name="email" placeholder="Informe o E-mail"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoafisica)) echo $pessoafisica['email']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telefone">Telefone <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" id="telefone" name="telefone" value="<?php if (isset($pessoafisica)) echo $pessoafisica['telefone']; ?>" data-inputmask="'mask' : '(99)9999-9999'" class="form-control" placeholder="Informe o Telefone">
                            </div>
                            <label class="control-label col-md-1 col-sm-2 col-xs-12" for="celular">Celular</label>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" value="<?php if (isset($pessoafisica)) echo $pessoafisica['celular']; ?>" data-inputmask="'mask' : '(99)99999-9999'" class="form-control" id="celular" name="celular" placeholder="Informe o Celular">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="usuario">Usuário <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" id="usuario" name="usuario" value="<?php if (isset($pessoafisica)) echo $pessoafisica['usuario']; ?>" maxlength="50" class="form-control" placeholder="Informe o Usuário">
                            </div>
                            <label class="control-label col-md-1 col-sm-2 col-xs-12" for="senha">Senha <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="password" maxlength="50" value="<?php if (isset($pessoafisica)) echo $pessoafisica['senha']; ?>" class="form-control" id="senha" name="senha" placeholder="Informe a Senha">
                            </div>
                        </div>
                        
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="datanascimento">Data Nascimento <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="date" id="data" name="datanascimento" autocomplete="off" value="<?php if (isset($pessoafisica)) echo date('Y-m-d',strtotime($pessoafisica['datanascimento'])); ?>" maxlength="10" class="form-control" placeholder="Informe a Data de Nascimento">
                            </div>
                            <label class="control-label col-md-1 col-sm-2 col-xs-12" for="idade">Idade <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" maxlength="3" value="<?php if (isset($pessoafisica)) echo $pessoafisica['idade']; ?>" disabled="" class="form-control" id="idade" name="idade" placeholder="Informe a Idade">
                                <input type="hidden" value="<?php if (isset($pessoafisica)) echo $pessoafisica['idade']; ?>" name="idadecalculada" id="idadecalculada"/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="rg">RG <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" id="rg" name="rg" value="<?php if (isset($pessoafisica)) echo $pessoafisica['rg']; ?>" data-inputmask="'mask' : '9999999999'" class="form-control" placeholder="Informe o RG">
                            </div>
                            <label class="control-label col-md-1 col-sm-2 col-xs-12" for="CPF">CPF <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" maxlength="14" value="<?php if (isset($pessoafisica)) echo $pessoafisica['cpf']; ?>" class="form-control" id="cpf" name="cpf" data-inputmask="'mask' : '999.999.999-99'" placeholder="Informe o CPF">
                            </div>
                        </div>
                        
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="status">Ativo/Status <span class="required">*</span>
                            </label>
                        <div class="form-group" style="margin-left: 19%;">
                            <label class="col-md-1"><div class="radio" id="uniform-undefined"><span><input type="radio" name="ativo" value="S" checked=""
                              <?php if(isset($pessoafisica)) if ($pessoafisica['ativo'] == 'S'){echo 'checked';}else{$pessoafisica == null;} ?>>Ativo</span></div></label>
                              <label><div class="radio" id="uniform-undefined"><span><input type="radio" name="ativo" value="N"
                              <?php if(isset($pessoafisica)) if ($pessoafisica['ativo'] == 'N'){echo 'checked';}else{$pessoafisica == null;} ?>>Nao Ativo</span></div></label>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-10">Observação</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <textarea class="form-control" rows="3" id="observacao" name="observacao" maxlength="2000" placeholder="Informe a Observação"><?php if (isset($pessoafisica)) echo $pessoafisica['observacao']; ?></textarea>
                            </div>
                        </div>
                        
                        <b style="font-size: 14px">&nbsp;&nbsp;&nbsp;Dados referente ao Endereço da Pessoa Física</b>
                        <br/><br/>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Cidade</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="select2_single form-control" name="idcidade" id="idcidade" required data-errormessage-value-missing="Selecione a Cidade">
                                  <option value="">Selecione a Cidade</option>
                                    <?php
                                    foreach ($listaCidades as $cidades) {
                                        $selected = (isset($pessoafisica) && $pessoafisica['idcidade'] == $cidades['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $cidades['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $cidades['nomecidade']; ?>
                                        </option>
                                    <?php } ?>
                              </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="bairro">Bairro <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="bairro" name="bairro" placeholder="Informe o Bairro"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoafisica)) echo $pessoafisica['bairro']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="rua">Rua <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="rua" name="rua" placeholder="Informe a Rua"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoafisica)) echo $pessoafisica['rua']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="numero">Número <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" id="numero" onkeypress="return Onlynumbers(event)" name="numero" maxlength="5" value="<?php if (isset($pessoafisica)) echo $pessoafisica['numero']; ?>" class="form-control" placeholder="Informe o Número">
                            </div>
                            <label class="control-label col-md-1 col-sm-2 col-xs-12" for="cep">CEP
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" maxlength="10" value="<?php if (isset($pessoafisica)) echo $pessoafisica['cep']; ?>" data-inputmask="'mask' : '99999-999'" class="form-control" id="cep" name="cep" placeholder="Informe o CEP">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="pontoreferencia">Ponto Referência
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="pontoreferencia" name="pontoreferencia" placeholder="Informe o Ponto de Referência"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoafisica)) echo $pessoafisica['pontoreferencia']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="complemento">Complemento
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="complemento" name="complemento" placeholder="Informe o Complemento"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoafisica)) echo $pessoafisica['complemento']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-10">Observação</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <textarea class="form-control" rows="3" id="observacaoendereco" name="observacaoendereco" maxlength="2000" placeholder="Informe a Observação"><?php if (isset($pessoafisica)) echo $pessoafisica['observacaoendereco']; ?></textarea>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                              <input type="submit" class="btn btn-success" value="Gravar" />
                              <?php
                                if($alterando == 1){?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-primary disabled" type="reset" value="Limpar" />
                                <?php } else {?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-info" type="reset" value="Limpar" />
                                <?php }
                              ?>
                          </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="src/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    jQuery.extend(jQuery.validator.messages, {
        email:"Por favor informe um e-mail válido."
    });
    $("#formPessoafisica").validate({
        rules: {
            nome: {
                required: true,
                minlength: 3,
                maxlength: 80
            },
            usuario: {
                required: true,
                minlength: 6,
                maxlength: 50
            },
            senha: {
                required: true,
                minlength: 6,
                maxlength: 50
            },
            email: {
                required: true,
                maxlength: 100
            },
            telefone: {
                required: true
            },
            datanascimento: {
                required: true
            },
            idade: {
                required: true
            },
            rg: {
                required: true
            },
            cpf: {
                required: true
            },
            idcidade: {
                required: true
            },
            bairro: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            rua: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            numero: {
                required: true
            }
        },
        messages: {
            nome: {
                required: "Por favor, informe o Nome",
                minlength: "O Nome deve ter pelo menos 3 caracteres",
                maxlength: "O Nome deve ter no máximo 80 caracteres"
            },
            usuario: {
                required: "Por favor, informe o Usuário",
                minlength: "O Nome deve ter pelo menos 6 caracteres",
                maxlength: "O Nome deve ter no máximo 50 caracteres"
            },
            senha: {
                required: "Por favor, informe a Senha",
                minlength: "A Senha deve ter pelo menos 6 caracteres",
                maxlength: "A Senha deve ter no máximo 50 caracteres"
            },
            email: {
                required: "Por favor, informe o E-mail",
                maxlength: "O E-mail deve ter no máximo 100 caracteres"
            },
            telefone: {
                required: "Por favor, informe o Telefone"
            },
            datanascimento: {
                required: "Por favor, informe a Data de Nascimento"
            },
            idade: {
                required: "Por favor, informe a Idade"
            },
            rg: {
                required: "Por favor, informe o RG"
            },
            cpf: {
                required: "Por favor, informe o CPF"
            },
            idcidade: {
                required: "Por favor, selecione a Cidade"
            },
            bairro: {
                required: "Por favor, informe o Bairro",
                minlength: "O Bairro deve ter pelo menos 3 caracteres",
                maxlength: "O Bairro deve ter no máximo 100 caracteres"
            },
            rua: {
                required: "Por favor, informe a Rua",
                minlength: "A Rua deve ter pelo menos 3 caracteres",
                maxlength: "A Rua deve ter no máximo 100 caracteres"
            },
            numero: {
                required: "Por favor, informe o Telefone"
            }
        }
    });
</script>

<script type="text/javascript">
    //Calcular Data
    document.getElementById("data").addEventListener('change', function() {
    var data = new Date(this.value);
    if(isDate_(this.value) && data.getFullYear() > 1900)
        document.getElementById("idade").value = calculateAge(this.value);
        document.getElementById("idadecalculada").value = calculateAge(this.value);
    });

    function calculateAge(dobString) {
    var dob = new Date(dobString);
    var currentDate = new Date();
    var currentYear = currentDate.getFullYear();
    var birthdayThisYear = new Date(currentYear, dob.getMonth(), dob.getDate());
    var age = currentYear - dob.getFullYear();
    if(birthdayThisYear > currentDate) {
      age--;
    }
    return age;
    }

    function calcular(data) {
    var data = document.form.formPessoafisica.value;
    alert(data);
    var partes = data.split("/");
    var junta = partes[2]+"-"+partes[1]+"-"+partes[0];
    document.form.idade.value = (calculateAge(junta));
    }

    var isDate_ = function(input) {
          var status = false;
          if (!input || input.length <= 0) {
            status = false;
          } else {
            var result = new Date(input);
            if (result == 'Invalid Date') {
              status = false;
            } else {
              status = true;
            }
          }
          return status;
    }
</script>