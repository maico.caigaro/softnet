<?php
    if(isset($_GET['updt']) == 'dXBkYXRlc3RhdHVz'){?>
        <div class="alert alert-success">
            Dados atualizados com <strong>Sucesso</strong>
        </div>
    <?php }else if(isset($_GET['updterror']) == 'dXBkYXRlZXJybw=='){?>
        <div class="alert alert-danger">
            Não foi possível atualizar o registro.
        </div>
    <?php }else if(isset($_GET['dlt']) == 'ZGVsZXRlc3VjZXNz'){?>
        <div class="alert alert-success">
            Registro Removido com <strong>Sucesso</strong>
        </div>
    <?php }else if(isset($_GET['dlterror']) == 'ZGVsZXRlZXJyb3I='){?>
        <div class="alert alert-danger">
            Não foi possível excluir o registro.
        </div>
    <?php }else if(isset($_GET['insert']) == 'aW5zZXJ0c3VjZXNz'){?>
        <div class="alert alert-success">
            Registro inserido com <strong>Sucesso</strong>
        </div>
    <?php }else if(isset($_GET['inserterror']) == 'aW5zZXJ0ZXJyb3I='){?>
        <div class="alert alert-danger">
            Não foi possível inserir o registro.
        </div>
    <?php }
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Relação de Pessoa Física</h2>
                <div class="clearfix"></div>
            </div>
            <a href="index.php?controle=pessoafisicaController&acao=novo">
                <span class='glyphicon glyphicon-plus' title="Adicionar Tipo de Serviço"> Adicionar</span>
            </a>
            <br/><br/>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>Contato</th>
                            <th>Cidade</th>
                            <th>Bairro</th>
                            <th>Ativo</th>
                            <th style="text-align: center">Agendamento</th>
                            <th style="text-align: center">Alterar</th>
                            <th style="text-align: center"style="text-align: center">Excluir</th>
                        </tr>
                    </thead>

                    <tbody> 
                        <?php
                            foreach ($listaDados as $item) {
                                echo '<td style="">' . $item['nomepessoa'];
                                echo '<td style="">' . $item['cpf'];
                                echo '<td style="">' . $item['contatopessoafisica'];
                                echo '<td style="">' . $item['nomecidade'];
                                echo '<td style="">' . $item['bairro'];
                                if($item['ativo'] == 'S'){
                                    $ativo = 'Sim';
                                }else{
                                    $ativo = 'Não';
                                }
                                echo '<td style="text-align: center">' . $ativo;

                                $id = base64_encode($item['id']);
                                echo "<td style='text-align: center;'> <a href='index.php?controle=agendamentoController&acao=novo&id=$id' title='Clique aqui para cadastrar um Agendamento'"
                                        . " class='btn btn-success btn-xs'><i class='fa fa-pencil'></i> Agendar </a>"
                                . "</a> </td>";
                                echo "<td style='text-align: center;'> <a href='index.php?controle=pessoafisicaController&acao=buscar&id=$id' title='Clique aqui para alterar o registro'"
                                        . " class='btn btn-info btn-xs'><i class='fa fa-pencil'></i> Alterar </a>"
                                . "</a> </td>";
                                echo "<td style='text-align: center;'><a href='index.php?controle=pessoafisicaController&acao=excluir&id=$id' class='btn btn-danger btn-xs'><i class='fa fa-trash-o'></i> Remover </a></td>";
                                echo '</tr>';
                            }
                        ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>