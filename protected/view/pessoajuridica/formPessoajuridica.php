<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cadastro de Pessoa Jurídica</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form action="<?php echo $acao; ?>" name="formPessoajuridica" id="formPessoajuridica" method="POST" role="form" data-parsley-validate class="form-horizontal form-label-left">
                        <?php
                            //Verificar se estou recebendo o ID, se estiver recebendo quer dizer que estou editando
                            if(isset($pessoajuridica['id']) != ''){
                                $alterando = 1;
                            }else{
                                $alterando = 0;
                            }
                        ?>
                        <input type="hidden" name="id" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['id']; ?>"/>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="razaosocial">Razão Social <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="razaosocial" name="razaosocial" placeholder="Informe a Razão Social"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['razaosocial']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nomefantasia">Nome Fantasia <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="nomefantasia" name="nomefantasia" placeholder="Informe o Nome Fantasia"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['nomefantasia']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="email">E-mail <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="email" name="email" placeholder="Informe o E-mail"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['email']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="telefone">Telefone <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" id="telefone" name="telefone" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['telefone']; ?>" data-inputmask="'mask' : '(99)9999-9999'" class="form-control" placeholder="Informe o Telefone">
                            </div>
                            <label class="control-label col-md-1 col-sm-2 col-xs-12" for="celular">Celular</label>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['celular']; ?>" data-inputmask="'mask' : '(99)99999-9999'" class="form-control" id="celular" name="celular" placeholder="Informe o Celular">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="usuario">Usuário <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" id="usuario" name="usuario" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['usuario']; ?>" maxlength="50" class="form-control" placeholder="Informe o Usuário">
                            </div>
                            <label class="control-label col-md-1 col-sm-2 col-xs-12" for="senha">Senha <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="password" maxlength="50" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['senha']; ?>" class="form-control" id="senha" name="senha" placeholder="Informe a Senha">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="dataabertura">Data Abertura <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" id="dataabertura" name="dataabertura" autocomplete="off" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['dataabertura']; ?>" class="form-control" placeholder="Informe a Data de Abertura">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="inscricaoestadual">Inscrição Estadual <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" id="inscricaoestadual" name="inscricaoestadual" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['inscricaoestadual']; ?>" data-inputmask="'mask' : '999.999.999.999'" data-inputmask="'mask' : '9999999999'" class="form-control" placeholder="Informe a Inscrição Estadual">
                            </div>
                            <label class="control-label col-md-1 col-sm-2 col-xs-12" for="cnpj">CNPJ <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['cnpj']; ?>" class="form-control" id="cnpj" name="cnpj" data-inputmask="'mask' : '99.999.999/9999-99'" placeholder="Informe o CPF">
                            </div>
                        </div>
                        
                        <label class="control-label col-md-2 col-sm-2 col-xs-12" for="status">Ativo/Status <span class="required">*</span>
                            </label>
                        <div class="form-group" style="margin-left: 19%;">
                            <label class="col-md-1"><div class="radio" id="uniform-undefined"><span><input type="radio" name="ativo" value="S" checked=""
                              <?php if(isset($pessoajuridica)) if ($pessoajuridica['ativo'] == 'S'){echo 'checked';}else{$pessoajuridica == null;} ?>>Ativo</span></div></label>
                              <label><div class="radio" id="uniform-undefined"><span><input type="radio" name="ativo" value="N"
                              <?php if(isset($pessoajuridica)) if ($pessoajuridica['ativo'] == 'N'){echo 'checked';}else{$pessoajuridica == null;} ?>>Nao Ativo</span></div></label>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-10">Observação</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <textarea class="form-control" rows="3" id="observacao" name="observacao" maxlength="2000" placeholder="Informe a Observação"><?php if (isset($pessoajuridica)) echo $pessoajuridica['observacao']; ?></textarea>
                            </div>
                        </div>
                        
                        <b style="font-size: 14px">&nbsp;&nbsp;&nbsp;Dados referente ao Endereço da Pessoa Jurídica</b>
                        <br/><br/>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Cidade</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                              <select class="select2_single form-control" name="idcidade" id="idcidade" required data-errormessage-value-missing="Selecione a Cidade">
                                  <option value="">Selecione a Cidade</option>
                                    <?php
                                    foreach ($listaCidades as $cidades) {
                                        $selected = (isset($pessoajuridica) && $pessoajuridica['idcidade'] == $cidades['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $cidades['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $cidades['nomecidade']; ?>
                                        </option>
                                    <?php } ?>
                              </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="bairro">Bairro <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="bairro" name="bairro" placeholder="Informe o Bairro"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['bairro']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="rua">Rua <span class="required">*</span>
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="rua" name="rua" placeholder="Informe a Rua"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['rua']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="numero">Número <span class="required">*</span>
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" id="numero" onkeypress="return Onlynumbers(event)" name="numero" maxlength="5" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['numero']; ?>" class="form-control" placeholder="Informe o Número">
                            </div>
                            <label class="control-label col-md-1 col-sm-2 col-xs-12" for="cep">CEP
                            </label>
                            <div class="col-md-4 col-sm-4 col-xs-8 form-group has-feedback">
                                <input type="text" maxlength="10" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['cep']; ?>" data-inputmask="'mask' : '99999-999'" class="form-control" id="cep" name="cep" placeholder="Informe o CEP">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="pontoreferencia">Ponto Referência
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="pontoreferencia" name="pontoreferencia" placeholder="Informe o Ponto de Referência"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['pontoreferencia']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="complemento">Complemento
                            </label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <input type="text" id="complemento" name="complemento" placeholder="Informe o Complemento"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($pessoajuridica)) echo $pessoajuridica['complemento']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-10">Observação</label>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                              <textarea class="form-control" rows="3" id="observacaoendereco" name="observacaoendereco" maxlength="2000" placeholder="Informe a Observação"><?php if (isset($pessoajuridica)) echo $pessoajuridica['observacaoendereco']; ?></textarea>
                            </div>
                        </div>
                      </div>
                      <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                              <input type="submit" class="btn btn-success" value="Gravar" />
                              <?php
                                if($alterando == 1){?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-primary disabled" type="reset" value="Limpar" />
                                <?php } else {?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-info" type="reset" value="Limpar" />
                                <?php }
                              ?>
                          </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="src/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="src/js/jquery-ui12.js"></script>
<script>
    $("#formPessoajuridica").validate({
        rules: {
            razaosocial: {
                required: true,
                minlength: 3,
                maxlength: 80
            },
            nomefantasia: {
                required: true,
                minlength: 3,
                maxlength: 80
            },
            email: {
                required: true,
                maxlength: 100
            },
            cnpj: {
                required: true,
                maxlength: 18
            },
            inscricaoestadual: {
                required: true,
                maxlength: 20
            },
            usuario: {
                required: true,
                minlength: 6,
                maxlength: 50
            },
            senha: {
                required: true,
                minlength: 6,
                maxlength: 50
            },
            telefone: {
                required: true
            },
            dataabertura: {
                required: true
            },
            idcidade: {
                required: true
            },
            bairro: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            rua: {
                required: true,
                minlength: 3,
                maxlength: 100
            },
            numero: {
                required: true
            }
        },
        messages: {
            razaosocial: {
                required: "Por favor, informe a Razão Social",
                minlength: "A Razão Social deve ter pelo menos 3 caracteres",
                maxlength: "A Razão Social deve ter no máximo 80 caracteres"
            },
            nomefantasia: {
                required: "Por favor, informe o Nome Fantasia",
                minlength: "O Nome Fantasia deve ter pelo menos 3 caracteres",
                maxlength: "O Nome Fantasia deve ter no máximo 80 caracteres"
            },
            usuario: {
                required: "Por favor, informe o Usuário",
                minlength: "O Nome deve ter pelo menos 6 caracteres",
                maxlength: "O Nome deve ter no máximo 50 caracteres"
            },
            senha: {
                required: "Por favor, informe a Senha",
                minlength: "A Senha deve ter pelo menos 6 caracteres",
                maxlength: "A Senha deve ter no máximo 50 caracteres"
            },
            email: {
                required: "Por favor, informe o E-mail",
                maxlength: "O E-mail deve ter no máximo 100 caracteres"
            },
            dataabertura: {
                required: "Por favor, informe a Data de Abertura"
            },
            telefone: {
                required: "Por favor, informe o Telefone"
            },
            inscricaoestadual: {
                required: "Por favor, informe a Inscrição Estadual"
            },
            cnpj: {
                required: "Por favor, informe o CNPJ"
            },
            idcidade: {
                required: "Por favor, selecione a Cidade"
            },
            bairro: {
                required: "Por favor, informe o Bairro",
                minlength: "O Bairro deve ter pelo menos 3 caracteres",
                maxlength: "O Bairro deve ter no máximo 100 caracteres"
            },
            rua: {
                required: "Por favor, informe a Rua",
                minlength: "A Rua deve ter pelo menos 3 caracteres",
                maxlength: "A Rua deve ter no máximo 100 caracteres"
            },
            numero: {
                required: "Por favor, informe o Telefone"
            }
        }
    });
</script>