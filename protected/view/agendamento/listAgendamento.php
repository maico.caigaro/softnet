<?php
    if(isset($_GET['insert']) == 'aW5zZXJ0c3VjZXNz'){?>
        <div class="alert alert-success">
            Agendamento concluído
        </div>
    <?php }else if(isset($_GET['inserterror']) == 'dXBkYXRlZXJybw=='){?>
        <div class="alert alert-danger">
            Não foi possível concluir o Agendamento.
        </div>
    <?php }
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Relação de Agendamento</h2>
                <div class="clearfix"></div>
            </div>
            <br/><br/>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Telefone</th>
                            <th>Rua/Bairro</th>
                            <th>Cidade</th>
                            <th>Tipo Serviço</th>
                            <th style="text-align: center;">Data Abertura</th>
                            <th style="text-align: center;">Finalizar</th>
                        </tr>
                    </thead>

                    <tbody> 
                        <?php
                            foreach ($listaDados as $item) {
                                echo '<td>' . $item['nomepessoa'];
                                echo '<td>' . $item['telefone'];
                                echo '<td>' . $item['rua'] . ' - ' . $item['bairro'];
                                echo '<td>' . $item['nomecidade'];
                                echo '<td style="text-align: center" title=" ' . $item['descricaocompletaservico'] . " " . $item['valorplano'] .'">' . $item['descricaoservico'];
                                echo '<td style="text-align: center">' . $item['dataabertura'];
                                $id = base64_encode($item['id']);
                                $statusfinaliza = $item['status'];
                                if($statusfinaliza == 'F'){
                                    $botaofinalizar = 'class="btn btn-warning btn-xs" disabled> <i class="fa fa-check-circle"></i> Finalizado';
                                    $title = "title='Agendamento finalizado'";
                                }else if($statusfinaliza == 'A'){
                                    $botaofinalizar = 'class="btn btn-info btn-xs"> <i class="fa fa-check-circle"></i> Agendado';
                                    $title = "title='Clique aqui para finalizar o Agendamento'";
                                }
                                echo "<td style='text-align: center;'> <a href='index.php?controle=agendamentoController&acao=finalizaragendamento&id=$id' $title"
                                        . "$botaofinalizar</a>"
                                . "</a> </td>";
                                echo '</tr>';
                            }
                        ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>