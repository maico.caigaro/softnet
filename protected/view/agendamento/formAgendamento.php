<?php
//Decodificar o parametro que veio pelo get
$idenderecobusca = base64_decode($_GET['id']);
?>
<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cadastro de Agendamento</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form action="<?php echo $acao; ?>" name="formAgendamento" id="formAgendamento" method="POST" role="form" data-parsley-validate class="form-horizontal form-label-left">
                        <?php
                        date_default_timezone_set('America/Sao_Paulo');
                        $dataabertura = date('d-m-Y');

                        if (isset($agendamento['id']) != '') {
                            $alterando = 1;
                        } else {
                            $alterando = 0;
                        }
                        ?>
                        <input type="hidden" name="id" value="<?php if (isset($agendamento)) echo $agendamento['id']; ?>"/>
                        <input type="hidden" name="idendereco" value="<?php echo $idenderecobusca; ?>"/>
                        <input type="hidden" name="dataabertura" value="<?php echo $dataabertura; ?>"/>
                        <input type="hidden" name="status" value="A"/>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="descricao">Descrição <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="descricao" name="descricao" placeholder="Informe a Descrição"
                                       autocomplete="off" maxlength="100" value="<?php if (isset($agendamento)) echo $agendamento['descricao']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="dataabertura">Data Abertura <span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-8">
                                <input type="text" id="dataabertura" name="dataabertura" value="<?php echo $dataabertura; ?>" disabled="" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Serviço</label>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <select class="select2_single form-control" name="idservico" id="idservico" required data-errormessage-value-missing="Selecione um Serviço">
                                    <option value="">Selecione o Serviço</option>
                                    <?php
                                    foreach ($listaServicos as $servicos) {
                                        $selected = (isset($agendamento) && $agendamento['idservico'] == $servicos['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $servicos['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $servicos['tipoplano'] . ' - ' . $servicos['velocidadeplano'] . ' - R$ ' . $servicos['valorplano']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Tipo Serviço</label>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <select class="select2_single form-control" name="idtiposervico" id="idtiposervico" required data-errormessage-value-missing="Selecione um Tipo de Serviço">
                                    <option value="">Selecione o Tipo de Serviço</option>
                                    <?php
                                    foreach ($listaTiposervicos as $tiposervicos) {
                                        $selected = (isset($agendamento) && $agendamento['idtiposervico'] == $tiposervicos['id']) ? 'selected' : '';
                                        ?>
                                        <option value='<?php echo $tiposervicos['id']; ?>'
                                                <?php echo $selected; ?>> 
                                                    <?php echo $tiposervicos['descricao']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="Turno">Turno Instalação<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12" style="top: 5px;">
                                <input type="radio" name="turnoinstalacao" value="M" checked=""> <label>Manhã &nbsp;&nbsp;</label>
                                <input type="radio" name="turnoinstalacao" value="T"> <label>Tarde &nbsp;&nbsp;</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="status">Status<span class="required">*</span>
                            </label>
                            <div class="col-md-3 col-sm-3 col-xs-8">
                                <input type="text" id="status" name="status" value="Agendado" disabled="" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                        <?php if ($alterando == 1) { ?>
                            <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                            <input class="btn btn-primary disabled" type="reset" value="Limpar" />
                        <?php } else { ?>
                            <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                            <input class="btn btn-info" type="reset" value="Limpar" />
                        <?php }
                        ?>

                        <input type="submit" class="btn btn-success" value="Gravar" />
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Relação de Agendamentos</h2>
                <div class="clearfix"></div>
            </div>
            <br/><br/>
            <div class="x_content">
                <table id="datatable" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Tipo de Serviço</th>
                            <th>Descrição</th>
                            <th style="text-align: center">Data Abertura</th>
                            <th style="text-align: center">Turno Instalação</th>
                            <th style="text-align: center">Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($listaDados as $item) {
                                echo '<td title="' . $item['descricaocompletaservico'] . " " . $item['valorplano'] .'">' . $item['descricaoservico'];
                                echo '<td>' . $item['descricaoagenda'];
                                echo '<td style="text-align: center">' . $item['dataabertura'];
                                if($item['turnoinstalado'] == "M"){
                                    $turnoinstalado = "Manhã";
                                }else if($item['turnoinstalado'] == "T"){
                                    $turnoinstalado = "Tarde";
                                }
                                echo '<td style="text-align: center">' . $turnoinstalado;
                                if($item['status'] == 'A'){
                                    $statusagendamento = '<button type="button" class="btn btn-info btn-xs"> <i class="fa fa-check-circle"></i> Agendado</button>';
                                }else{
                                    $statusagendamento = '<button type="button" class="btn btn-success btn-xs"> <i class="fa fa-check-circle"></i> Finalizado</button>';
                                }
                                echo '<td style="text-align: center">' . $statusagendamento;
                                echo '</tr>';
                            }
                        ?>
                  </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<script src="src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="src/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $("#formAgendamento").validate({
        rules: {
            descricao: {
                required: true,
                minlength: 3,
                maxlength: 80
            },
            dataabertura: {
                required: true
            },
            idtiposervico: {
                required: true
            }
        },
        messages: {
            descricao: {
                required: "Por favor, informe a Descrição do Agendamento",
                minlength: "A Descrição deve ter pelo menos 3 caracteres",
                maxlength: "A Descrição deve ter no máximo 80 caracteres"
            },
            dataabertura: {
                required: "Por favor, a Data de Abertura deve ser informada"
            },
            idtiposervico: {
                required: "Por favor, selecione o Tipo de Serviço"
            }
        }
    });
</script>