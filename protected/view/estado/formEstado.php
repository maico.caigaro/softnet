<div class="">
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cadastro de Estado</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form action="<?php echo $acao; ?>" name="formEstado" id="formEstado" method="POST" role="form" data-parsley-validate class="form-horizontal form-label-left">
                        <?php
                            if(isset($estado['id']) != ''){
                                $alterando = 1;
                            }else{
                                $alterando = 0;
                            }
                        ?>
                        <input type="hidden" name="id" value="<?php if (isset($estado)) echo $estado['id']; ?>"/>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="nome">Nome <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="nome" name="nome" placeholder="Informe o Nome"
                                       autocomplete="off" onkeypress="return Onlychars(event)" maxlength="100" value="<?php if (isset($estado)) echo $estado['nome']; ?>" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12" for="uf">UF <span class="required">*</span>
                            </label>
                            <div class="col-md-2 col-sm-2 col-xs-6">
                                <input type="text" id="uf" name="uf" placeholder="Informe a UF"
                                       autocomplete="off" maxlength="2" onkeypress="return Onlychars(event)" value="<?php if (isset($estado)) echo $estado['uf']; ?>" class="form-control col-md-2 col-xs-6">
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                              <input type="submit" class="btn btn-success" value="Gravar" />
                              <?php
                                if($alterando == 1){?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-primary disabled" type="reset" value="Limpar" />
                                <?php } else {?>
                                    <a class="btn btn-primary" href="javascript:window.history.go(-1)" role="button">Cancelar</a>
                                    <input class="btn btn-info" type="reset" value="Limpar" />
                                <?php }
                              ?>
                          </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="src/js/jquery-2.1.4.min.js" type="text/javascript"></script>
<script src="src/js/jquery.validate.min.js" type="text/javascript"></script>
<script>
    $("#formEstado").validate({
        rules: {
            nome: {
                required: true,
                minlength: 3,
                maxlength: 50
            },
            uf: {
                required: true,
                minlength: 2,
                maxlength: 2
            }
        },
        messages: {
            nome: {
                required: "Por favor, informe o Nome",
                minlength: "O Nome deve ter pelo menos 3 caracteres",
                maxlength: "O Nome deve ter no máximo 50 caracteres"
            },
            uf: {
                required: "Por favor, informe a UF",
                minlength: "A UF deve ter pelo menos 2 caracteres",
                maxlength: "A UF deve ter no máximo 2 caracteres"
            }
        }
    });
</script>