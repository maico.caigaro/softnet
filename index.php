<!DOCTYPE html>
<?php
    require_once './protected/libs/controlador.php';
    //CHAMADA AO BANCO DE DADOS
    include './config/confloginrel.php';
    include './config/confloginrel.php';
    
    //Verifica se tem usuário na sessão se não tiver direciona para o login
    session_start();
    if ($_SESSION['usuario'] == null) {
        header('location:acesso/login.php');
    }
    
    //Consulta o nome do usuário atraves da seção
    $sqlconsultausuario = pg_query("select nome as nomeusuario,
                                           id as idpessoa
                                      from pessoa 
                                     where usuario = " . "'" . $_SESSION['usuario'] . "'");
    $ressqlconsultausuario = pg_fetch_array($sqlconsultausuario);
    $nomeusuario = $ressqlconsultausuario['nomeusuario'];
    $idpessoa = $ressqlconsultausuario['idpessoa'];
?>
<html lang="pt-br">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />
    <title>SoftNet </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="src/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="src/css/custom.css"/>
    <link rel="stylesheet" href="src/css/jquery-ui.css" type="text/css" />
    
    <!-- Minhas Formatações CSS -->
    <link href="src/css/my_style.css" rel="stylesheet">
    
    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <link href="vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <link href="vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="vendors/normalize-css/normalize.css" rel="stylesheet">
    <link href="vendors/ion.rangeSlider/css/ion.rangeSlider.css" rel="stylesheet">
    <link href="vendors/ion.rangeSlider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
    <link href="vendors/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="src/css/custom.min.css" rel="stylesheet">
    
    <script src="src/js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="src/js/jquery-ui.js" type="text/javascript"></script>
    <script src="src/js/jquery.maskMoney.js" type="text/javascript"></script>
    <script src="src/js/jquery.maskedinput.min.js" type="text/javascript"></script>
    <script src="src/js/bootstrap.min.js"></script>
    <script src="src/js/mascaras_softnet.js"></script>
    <script>
        //Modal para relatórios
        function abrir(URL) {
            var width = 590;
            var height = 350;
            var left = 380;
            var top = 200;
            window.open(URL, 'janela', 'width=' + width +
                    ', height=' + height +
                    ', top=' + top +
                    ', left=' + left +
                    ', scrollbars=yes, \n\
                             status=no, \n\
                             toolbar=no, \n\
                             location=no, \n\
                             directories=no, \n\
                             menubar=no, \n\
                             resizable=no, \n\
                             fullscreen=no');
            }
    </script>
  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="index.html" class="site_title"><span>SoftNet</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                  <img src="src/images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Seja Bem Vindo,</span>
                <h2><?php echo $nomeusuario;?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                  <li><a href="index.php?controle=agendamentoController&acao=listar"><i class="fa fa-calendar"></i>Agendamento</a></li>
                  <li><a><i class="fa fa-user"></i> Cadastro de Pessoa <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="index.php?controle=pessoafisicaController&acao=listar"><i class="fa fa-user"></i> Pessoa Física</a>
                        <li><a href="index.php?controle=pessoajuridicaController&acao=listar"><i class="fa fa-user"></i> Pessoa Jurídica</a>
                        <li><a href="index.php?controle=inadimplentesController&acao=listar"><i class="fa fa-user"></i>Inadimplentes</a>
                    </ul>
                  </li>
                  <li><a href="index.php?controle=tiposervicoController&acao=listar"><i class="fa fa fa-gears"></i> Tipo de Serviço</a>
                  <li><a href="index.php?controle=servicoController&acao=listar"><i class="fa fa-gear"></i>Serviço</a>
                  <li><a href="index.php?controle=contratoController&acao=listar"><i class="fa fa-file-text"></i>Contrato</a></li>
                  <li><a href="index.php?controle=boletoController&acao=listar"><i class="fa fa-barcode"></i>Boleto</a></li>
                  <li><a href="javascript:abrir('protected/view/relatorios/filtro_rel_pessoa_plano.php');" target="_blank"><i class="fa fa-list-alt"></i>Relatório Pessoa/Plano</a></li>
                  <li><a href="index.php?controle=cidadeController&acao=listar"><i class="fa fa-plane"></i> Cidade</a>
                  <li><a href="index.php?controle=estadoController&acao=listar"><i class="fa fa-plane"></i> Estado</a>
                </ul>
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Settings">
                      <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="FullScreen">
                      <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="" data-original-title="Lock">
                      <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="" href="login.html" data-original-title="Logout">
                      <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                  </div>
              </div>
            </div>
          </div>  
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="src/images/img.jpg" alt=""><?php echo $nomeusuario?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="acesso/logout.php"><i class="fa fa-sign-out pull-right"></i> Sair</a></li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php
                    $app = new Controlador();
                ?>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
      </div>
    
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="src/js/custom.min.js"></script>
    
    <!-- Datatables -->
    <script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="vendors/jszip/dist/jszip.min.js"></script>
    <script src="vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    
    <script>
        $('#datatable').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "oLanguage": {
                        "sProcessing":   "Aguarde enquanto os dados são carregados ...",
                        "sLengthMenu":   "Mostrar _MENU_ registros por pagina",
                        "sZeroRecords":  "Nenhum registro correspondente ao criterio encontrado",
                        "sInfoEmtpy":    "Exibindo 0 a 0 de 0 registros",
                        "sInfo":         "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                        "sInfoFiltered": "",
                        "sSearch": "Pesquisar",
                            "oPaginate": {
                                "sFirst":    "Primeiro",
                                "sPrevious": "Anterior",
                                "sNext":     "Próximo",
                                "sLast":     "Último"
                        }  
                } 
            });
    </script>
  </body>
</html>
